package com.precisource.sdk.web.test;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.precisource.sdk.web.common.domain.dto.SpInspCheckDTO;
import com.precisource.sdk.web.common.domain.entity.Doctor;
import com.precisource.sdk.web.common.domain.entity.UserInspCheck;
import com.precisource.sdk.web.mappers.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSpInspShceck {

    @Autowired
    UserInspCheckMapper userInspCheckMapper;

    @Autowired
    RxInspDetlMapper rxInspDetlMapper;

    @Autowired
    DoctorMapper doctorMapper;

    @Autowired
    DoctorInspPacsDetailMapper doctorInspPacsDetailMapper;

    private static Logger logger = LoggerFactory.getLogger(TestSpInspShceck.class);

    @Test
    public void getSpInspCheckSingleTest() {
        String pid = "364977";
        List<SpInspCheckDTO> results = userInspCheckMapper.getSpInspCheckSingle(pid);
        logger.info(JSON.toJSONString(results, true));
    }


    @Test
    public void t1() {
        String pid = "364977";
        List<SpInspCheckDTO> results = userInspCheckMapper.getSpInspCheckNew(pid);
        logger.info("记录条数:{},查询结果:{}",results.size(),JSON.toJSONString(results, true));
    }

    @Test
    public void t2() {
        UserInspCheck userInspCheck = userInspCheckMapper.selectByPrimaryKey("5bf2b3d1e591c66da558c415");
        System.out.println(JSON.toJSONString(userInspCheck,true));
    }

    @Test
    public void t3() {
    }


}


