package com.precisource.sdk.web.enums;

/**
 * 报告类型
 *
 * @author xinput
 * @date 2018-07-02 22:05
 */
public enum ReportTypeEnum {
    LIS("1", "Lis检验"),
    PACS("2", "Pacs检查"),
    GENE("3", "基因-暂时未用");

    private String reportType;
    private String desc;

    ReportTypeEnum(String reportType, String desc) {
        this.reportType = reportType;
        this.desc = desc;
    }

    public String getReportType() {
        return reportType;
    }

    public String getDesc() {
        return desc;
    }
}
