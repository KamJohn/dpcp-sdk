package com.precisource.sdk.web.utils;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-04-04 10:29
 * @description
 */
public class CommonUtils {

    public static <T, R> List projectField(Collection<T> collection, Function<T, R> function) {
        return collection.stream()
                .map(function)
                .distinct()
                .collect(Collectors.toList());
    }
}
