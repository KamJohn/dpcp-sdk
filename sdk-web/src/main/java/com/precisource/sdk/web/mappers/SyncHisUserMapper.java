package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.SyncHisUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SyncHisUserMapper {
    int insertSelective(SyncHisUser record);

    List<SyncHisUser> getSyncHisUserInUserCodes(@Param("list") List<String> userCodes);
}