package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.UserBasic;
import org.apache.ibatis.annotations.Param;

public interface UserBasicMapper {

    int insertSelective(UserBasic record);

    UserBasic selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserBasic record);

    UserBasic getUserBasicByFlagAndUserId(@Param("flag") int regFlag, @Param("userId") String userId);
}