package com.precisource.sdk.web.consts;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author cerpengxi
 * @date 17/5/18 下午6:02
 */

public class Consts {

    public static final String COMMA = ",";

    public static final int RECORD_STATE_VALID = 0;

    /**
     * "-1" 代表暂无对应报告
     */
    public static final String NO_BIND_REPORD_CODE = "-1";

    @Value("default.hospitalId")
    public static String DEFAULT_HOSPITAL_ID;

    public enum UserStage {

        CONSULT("consult"),
        CLINIC("clinic"),
        SIGN("sign"),
        IVF("ivf"),
        ;
        private String value;

        UserStage(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }

        public static UserStage get(String value) {
            if (StringUtils.isNoneEmpty(value)) {
                return null;
            }
            for (UserStage userStage : UserStage.values()) {
                if (value.equals(userStage.value())) {
                    return userStage;
                }
            }
            return null;
        }

        public boolean isAfter(UserStage userStage) {
            if (userStage == null) {
                return false;
            }
            if (ordinal() > userStage.ordinal()) {
                return true;
            }
            return false;
        }

    }

    public enum UserType {
        USER("user"),
        DOCTOR("doctor");

        private String value;

        UserType(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

    public enum RecordState {
        VALID(0),
        INVALID(1);

        private int value;

        RecordState(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public enum InquiryState {

        WAITING(0),
        PROCESSING(1),
        DONE(2),
        ;

        private int value;

        InquiryState(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }
    }

    public enum RegistFlag {
        Regist(1, "注册用户"),
        FamilyMember(2, "家庭成员");

        private int regFlag;
        private String desc;

        RegistFlag(int regFlag, String desc) {
            this.regFlag = regFlag;
            this.desc = desc;
        }

        public int getRegFlag() {
            return regFlag;
        }

        public String getDesc() {
            return desc;
        }
    }

}
