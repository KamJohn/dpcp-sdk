package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.DoctorInspPacsDetailWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DoctorInspPacsDetailMapper {

    int insertSelective(DoctorInspPacsDetailWithBLOBs record);

    int updateByPrimaryKeySelective(DoctorInspPacsDetailWithBLOBs record);

    long countByUserCodeAndPid(@Param("hisId") String hisId, @Param("pid") String pid);

    void deleteByUserCodeAndPid(@Param("hisId") String hisId, @Param("pid") String pid);

    void batchInsertDoctorInspectPacsDetail(@Param("list") List<Map<String, Object>> pacsDetailParams);
}