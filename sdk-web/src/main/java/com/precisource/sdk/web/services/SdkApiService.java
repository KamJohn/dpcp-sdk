package com.precisource.sdk.web.services;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.precisource.data.clients.XiangYaClient;
import com.precisource.data.model.dpcp.ZxxyEmrA01;
import com.precisource.data.model.dpcp.ZxxyEmrA02;
import com.precisource.data.model.dpcp.ZxxyYyglE61;
import com.precisource.data.model.dpcp.ZxxyYyglE62;
import com.precisource.data.model.vo.BaseVO;
import com.precisource.data.model.vo.YYGLY02DTO;
import com.precisource.data.model.xiangyaws.req.WsAppReportLISReq;
import com.precisource.data.model.xiangyaws.req.WsAppReportPACSReq;
import com.precisource.data.model.xiangyaws.req.WsAppReportReq;
import com.precisource.data.model.xiangyaws.req.doctor.WsHisLisAllReq;
import com.precisource.data.model.xiangyaws.req.doctor.WsHisReportPACSReq;
import com.precisource.data.model.xiangyaws.req.doctor.WsHisReportReq;
import com.precisource.data.model.xiangyaws.resp.*;
import com.precisource.data.model.xiangyaws.resp.all.ResultRow;
import com.precisource.data.model.xiangyaws.resp.all.WsHisLisAllResp;
import com.precisource.data.model.xiangyaws.resp.all.WsHisLisAllResult;
import com.precisource.sdk.web.common.domain.dto.SpInspCheckDTO;
import com.precisource.sdk.web.common.domain.entity.*;
import com.precisource.sdk.web.consts.Consts;
import com.precisource.sdk.web.enums.ReportTypeEnum;
import com.precisource.sdk.web.enums.RxTypeEnum;
import com.precisource.sdk.web.enums.WsidTypeEnum;
import com.precisource.sdk.web.utils.DateUtils;
import com.precisource.sdk.web.utils.LocalDateUtils;
import com.precisource.sdk.web.utils.ObjectIdUtils;
import com.precisource.sdk.web.mappers.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.precisource.sdk.web.utils.PropertiesUtils.propertiesUtil;

/**
 * 访问封装的sdk服务
 *
 * @author xinput
 * @date 2018-07-02 10:53
 */
@Service
public class SdkApiService {

    @Autowired
    private RxRecordMapper rxRecordMapper;

    @Autowired
    private RxInspDetlMapper rxInspDetlMapper;

    @Autowired
    private RxDrugDetlMapper rxDrugDetlMapper;

    @Autowired
    private EmrRecordMapper emrRecordMapper;

    @Autowired
    private EmrDicMapper emrDicMapper;

    @Autowired
    private SyncHisUserMapper syncHisUserMapper;

    @Autowired
    private EmrRecordDetlMapper emrRecordDetlMapper;

    @Autowired
    private EmrDicDetlMapper emrDicDetlMapper;

    @Autowired
    private InspGeneProcessMapper inspGeneProcessMapper;

    @Autowired
    private UserMappingMapper userMappingMapper;

    @Autowired
    private DoctorInspListMapper doctorInspListMapper;

    @Autowired
    private DoctorInspPacsDetailMapper doctorInspPacsDetailMapper;

    @Autowired
    private DoctorInspLisDetailMapper doctorInspLisDetailMapper;

    @Autowired
    private UserInspCheckMapper userInspCheckMapper;

    @Autowired
    private InspListMapper inspListMapper;

    @Autowired
    private InspLisDetailMapper inspLisDetailMapper;

    @Autowired
    private UserNodeTimeRecordMapper userNodeTimeRecordMapper;

    public static String XY_BASE_URL = propertiesUtil.getStringProperty("xiangya.baseUrl");

    private static String XY_ACCESS_KEY = propertiesUtil.getStringProperty("xiangya.access.key");

    private static String XY_ACCESS_SECRET = propertiesUtil.getStringProperty("xiangya.access.secret");

    private String XY_WS_DYBH = propertiesUtil.getStringProperty("xiangya.ws.dybh");

    private String XY_WS_DYMM = propertiesUtil.getStringProperty("xiangya.ws.dymm");

    private static String XIANGYA_REPORT_LIS_FYJG = propertiesUtil.getStringProperty("xiangya.report.lis.fyjg");

    private static final List<String> FYJGS = Arrays.asList(XIANGYA_REPORT_LIS_FYJG.replaceAll(" ", StringUtils.EMPTY)
            .split(Consts.COMMA)
    ).stream().collect(Collectors.toList());


    private static final Logger LOGGER = LoggerFactory.getLogger(SdkApiService.class);

    private static final XiangYaClient xiangyaClient = new XiangYaClient(XY_BASE_URL, XY_ACCESS_KEY, XY_ACCESS_SECRET);

    /**
     * @param pid, nowDate [ 用户PID、 参数日期]
     * @desc 根据pid同步用户数据，对于对于用户病例信息，因为在我们的数据库中也是根据pid和 对应的启峰用户id 查询，所以先根据pid删除后增，不会影响我们本地库
     */
    void syncCaseByPid(String pid, Date nowDate) throws Exception {
        LOGGER.info("同步用户数据: case. pid={}", pid);
        Stopwatch stopwatch = Stopwatch.createStarted();
        BaseVO<ZxxyEmrA01, ZxxyEmrA02> baseVO = xiangyaClient.getCases(pid);
        if (null == baseVO) {
            String errMsg = String.format("同步用户数据: case. pid=%s,病例信息返回值为null.", pid);
            LOGGER.error(errMsg);
            throw new RuntimeException(errMsg);
        }
        List<ZxxyEmrA01> emrA01s = baseVO.getKs();
        List<ZxxyEmrA02> emrA02s = baseVO.getVs();
        if (CollectionUtils.isEmpty(emrA01s) || CollectionUtils.isEmpty(emrA02s)) {
            LOGGER.info("同步用户数据: case. pid={},病例信息为空", pid);
            return;
        }

        // 组装病历记录表参数、病历明细表参数
        List<Map<String, Object>> emrRecordParams = this.makeEmrRecordParams(emrA01s, nowDate);
        List<Map<String, Object>> emrRecordDetailParams = this.makeEmrRecordDetailParams(emrA02s, nowDate);

        // 取出最大值和最小值
        List<LocalDate> dates = emrRecordParams.stream().map(emrRecordParam -> LocalDateUtils.dateToLocalDate((Date) emrRecordParam.get("recordTime"))).collect(Collectors.toList());
        LocalDate maxDate = dates.stream().max(Comparator.comparing(LocalDate::toEpochDay)).get();
        LocalDate minDate = dates.stream().min(Comparator.comparing(LocalDate::toEpochDay)).get();

        try {
            long countEmr = emrRecordMapper.countByPid(pid);
            if (countEmr > 0) {
                emrRecordMapper.deleteByPid(pid);
            }
            if (!CollectionUtils.isEmpty(emrRecordParams)) {
                emrRecordMapper.batchInsertEmrRecord(emrRecordParams);
            }

            long countEmrDetl = emrRecordDetlMapper.countByPid(pid);
            if (countEmrDetl > 0) {
                emrRecordDetlMapper.deletByPid(pid);
            }
            if (!CollectionUtils.isEmpty(emrRecordDetailParams)) {
                emrRecordDetlMapper.batchInsertEmrRecordDetail(emrRecordDetailParams);
            }
            LOGGER.info("同步用户数据: case,更新完成,commit. pid={}, EmrRecord: delete={}, add={}.EmrRecordDetl: delete={}, add={}", pid, countEmr, emrRecordParams.size(), countEmrDetl, emrRecordDetailParams.size());

            // 更新
            long countUserNodeTimeRecord = userNodeTimeRecordMapper.countByPid(pid);
            if (countUserNodeTimeRecord > 0) {
                userNodeTimeRecordMapper.deleteByPid(pid);
            }
            UserNodeTimeRecord userNodeTimeRecord = new UserNodeTimeRecord();
            userNodeTimeRecord.setId(ObjectIdUtils.stringId());
            userNodeTimeRecord.setPid(pid);
            userNodeTimeRecord.setFirstVisitDate(Date.from(minDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            userNodeTimeRecord.setLastVisitDate(Date.from(maxDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            userNodeTimeRecord.setCreateTime(nowDate);
            userNodeTimeRecord.setUpdateTime(nowDate);
            userNodeTimeRecord.setRecordState(Consts.RECORD_STATE_VALID);
            userNodeTimeRecordMapper.addUserNodeTimeRecord(userNodeTimeRecord);
        } catch (Exception e) {
            String errMsg = String.format("同步用户数据: case,更新失败，rollback. pid=%s,rollback.", pid);
            LOGGER.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        stopwatch.stop();
        LOGGER.info("同步用户数据: case. pid={},cost:{} mills ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * @param pid, nowDate [ 用户PID、时间参数 ]
     * @desc 根据pid同步用户开单信息
     */
    void syncOrderByPid(String pid, Date nowDate) throws Exception {
        LOGGER.info("同步用户数据: order. pid={}", pid);
        Stopwatch stopwatch = Stopwatch.createStarted();

        BaseVO<ZxxyYyglE61, ZxxyYyglE62> baseVO = xiangyaClient.getOrders(pid);
        if (null == baseVO) {
            String errMsg = String.format("同步用户数据: order. 用户开单信息返回为null。pid:%s", pid);
            LOGGER.error("同步用户数据: order. pid={}, exception{}", pid, errMsg);
            throw new RuntimeException(errMsg);
        }

        List<Map<String, Object>> params = this.getRxRecord(pid, baseVO.getKs(), nowDate);
        Map<String, List<Map<String, Object>>> map = this.getRxDetail(pid, baseVO.getKs(), baseVO.getVs(), nowDate);
        // 处方检查明细
        List<Map<String, Object>> recordDetailParams = map.get("recordDetailParams");
        // 同步处方药品明细和
        List<Map<String, Object>> drugParams = map.get("drugParams");

        if (CollectionUtils.isEmpty(params) && CollectionUtils.isEmpty(recordDetailParams) && CollectionUtils.isEmpty(drugParams)) {
            LOGGER.info("同步用户数据: order. 开单信息为空，pid={}", pid);
            return;
        }
        try {
            long countRx = rxRecordMapper.countByPid(pid);
            if (countRx > 0) {
                rxRecordMapper.deleteByPid(pid);
            }
            if (!CollectionUtils.isEmpty(params)) {
                rxRecordMapper.batchInsertRxRecord(params);
            }

            long countRxDetl = rxInspDetlMapper.countRxRecordDetailByPid(pid);
            if (countRxDetl > 0) {
                rxInspDetlMapper.deleteRxRecordDetailByPid(pid);
            }
            if (!CollectionUtils.isEmpty(recordDetailParams)) {
                rxInspDetlMapper.batchInsertRecordDetail(recordDetailParams);
            }
            long countRxDrugDetl = rxDrugDetlMapper.countRxDrugDetlByPid(pid);
            if (countRxDrugDetl > 0) {
                rxDrugDetlMapper.deleteRxDrugDetlByPid(pid);
            }
            if (!CollectionUtils.isEmpty(drugParams)) {
                rxDrugDetlMapper.batchInsertRxDrugDetail(drugParams);
            }
            LOGGER.info("同步用户数据: order,更新完成,commit. pid={}. RxRecord: delete={}, add={}; RxRecordDetail: delete={}, add={}; RxDrugDetl: delete={}, add={}.", pid, countRx, params.size(), countRxDetl, recordDetailParams.size(), countRxDrugDetl, drugParams.size());
        } catch (Exception e) {
            String errMsg = String.format("同步用户数据: order. 更新失败,rollback. pid=%s,rollback.", pid);
            LOGGER.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        stopwatch.stop();
        LOGGER.info("同步用户数据: order. pid={},cost:{} mills ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 根据pid同步用户遗传报告检查进度数据
     *
     * @param pid
     */
    void syncGeneReportByPid(String pid, Date date) throws Exception {
        LOGGER.info("同步用户数据: 遗传报告进度数据. pid={}", pid);
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<YYGLY02DTO> y02s = xiangyaClient.queryYYGLY02s(pid);
        if (CollectionUtils.isEmpty(y02s)) {
            LOGGER.info("同步用户数据: 同步用户遗传报告检查进度数据. 开单信息为空，pid={}", pid);
            return;
        }

        List<Map<String, Object>> geneProgressParams = this.makeGeneProgressParam(y02s, date);
        if (CollectionUtils.isEmpty(geneProgressParams)) {
            LOGGER.info("同步用户数据: 同步用户遗传报告检查进度数据. 组装后参数为空，pid={}", pid);
            return;
        }

        try {
            long countProcess = inspGeneProcessMapper.countByPid(pid);
            if (countProcess > 0) {
                inspGeneProcessMapper.delete(pid);
            }
            if (!CollectionUtils.isEmpty(geneProgressParams)) {
                inspGeneProcessMapper.batchInsertInspGeneProcess(geneProgressParams);
            }
            LOGGER.info("同步用户数据: 同步用户遗传报告检查进度数据,更新完成,commit. pid={}. InspGeneProcess: delete={}, add={}.", pid, countProcess, geneProgressParams.size());
        } catch (Exception e) {
            String errMsg = String.format("同步用户数据: 同步用户遗传报告检查进度数据. 更新失败,rollback. pid=%s,rollback.", pid);
            LOGGER.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        stopwatch.stop();
        LOGGER.info("同步用户数据: 同步用户遗传报告检查进度数据. pid={},cost:{} mills ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 用户处方信息参数
     *
     * @param pid
     * @param yyglE61s
     */
    private List<Map<String, Object>> getRxRecord(String pid, List<ZxxyYyglE61> yyglE61s, Date nowDate) {
        if (null == yyglE61s || yyglE61s.size() == 0) {
            LOGGER.info("同步用户数据:order。RxRecord: pid={},数据为空", pid);
            return null;
        }

        List<Map<String, Object>> params = Lists.newArrayListWithCapacity(yyglE61s.size());
        yyglE61s.forEach(e61 -> {
            Map<String, Object> param = Maps.newHashMapWithExpectedSize(15);
            param.put("id", ObjectIdUtils.stringId());
            param.put("pid", e61.getPid());
            param.put("userCode", e61.getSbrxx01());
            param.put("registerCode", e61.getScfxx02());
            param.put("doctorCode", e61.getScfxx03());

            param.put("rxCode", e61.getScfxx01());
            param.put("rxDate", e61.getDcfxx04());
            param.put("rxType", e61.getScfxx06());
            param.put("diagnosis", e61.getSblxx10());
            param.put("createTime", nowDate);

            param.put("updateTime", nowDate);
            param.put("recordState", Consts.RECORD_STATE_VALID);
            param.put("templateId", e61.getScfxx15());

            params.add(param);
        });

        return params;
    }

    /**
     * 同步处方药品明细和处方检查明细
     *
     * @param pid
     * @param e61s
     * @param e62s
     */
    private Map<String, List<Map<String, Object>>> getRxDetail(String pid, List<ZxxyYyglE61> e61s, List<ZxxyYyglE62> e62s, Date nowDate) {
        Map<String, List<Map<String, Object>>> map = new HashMap<>();
        if (null == e61s || null == e62s || e61s.size() == 0 || e62s.size() == 0) {
            LOGGER.info("同步用户数据: order. RxDetail: pid={},数据为空", pid);
            map.put("recordDetailParams", null);
            map.put("drugParams", null);
            return map;
        }

        Map<String, ZxxyYyglE61> e61Maps = e61s.stream().collect(Collectors.toMap(ZxxyYyglE61::getScfxx01, id -> id, (k1, k2) -> k1));
        BigDecimal defalutNum = new BigDecimal(0);
        List<Map<String, Object>> recordDetailParams = Lists.newArrayListWithCapacity(e62s.size());
        List<Map<String, Object>> drugParams = Lists.newArrayListWithCapacity(e62s.size());
        e62s.forEach(e62 -> {
            ZxxyYyglE61 e61 = e61Maps.getOrDefault(e62.getScfxx01(), new ZxxyYyglE61());
            if (RxTypeEnum.RX_TYPE_01.getRxType().equals(e61.getScfxx06())
                    || RxTypeEnum.RX_TYPE_02.getRxType().equals(e61.getScfxx06())) {
                // 药品明细
                Map<String, Object> drugParam = Maps.newHashMapWithExpectedSize(25);
                drugParam.put("id", ObjectIdUtils.stringId());
                drugParam.put("rxCode", e62.getScfxx01());
                drugParam.put("rxDetlCode", e62.getNcfmx01());
                drugParam.put("rxType", e61.getScfxx06());
                drugParam.put("pid", pid);

                drugParam.put("userCode", e61.getSbrxx01());
                drugParam.put("drugCode", e62.getScfmx03());
                drugParam.put("drugName", e62.getScfmx04());
                drugParam.put("drugSpec", e62.getScfmx05());
                drugParam.put("drugQty", null == e62.getNcfmx06() ? defalutNum : e62.getNcfmx06());

                drugParam.put("drugQtyUnit", e62.getScfmx07());
                drugParam.put("drugPrice", null == e62.getNcfmx08() ? defalutNum : e62.getNcfmx08());
                drugParam.put("drugDosage", null == e62.getNcfmx09() ? defalutNum : e62.getNcfmx09());
                drugParam.put("drugDosageUnit", e62.getScfmx10());
                drugParam.put("drugUsage", e62.getScfmx11());

                drugParam.put("drugFreq", e62.getScfmx12());
                drugParam.put("drugTimes", null == e62.getNcfmx13() ? 0 : e62.getNcfmx13());
                drugParam.put("drugDays", null == e62.getNcfmx14() ? defalutNum : e62.getNcfmx14());
                drugParam.put("doctorAdvice", e62.getScfmx17());
                drugParam.put("paymentState", e62.getScfmx20());

                drugParam.put("comment", e62.getScfmx36());
                drugParam.put("executeState", e62.getScfmx44());
                drugParam.put("createTime", nowDate);
                drugParam.put("updateTime", nowDate);
                drugParam.put("recordState", Consts.RECORD_STATE_VALID);

                drugParams.add(drugParam);
            } else {
                // 处方明细
                Map<String, Object> recordDetailParam = Maps.newHashMapWithExpectedSize(20);
                recordDetailParam.put("id", ObjectIdUtils.stringId());
                recordDetailParam.put("rxCode", e62.getScfxx01());
                recordDetailParam.put("rxDetlCode", e62.getNcfmx01());
                recordDetailParam.put("rxType", e61.getScfxx06());
                recordDetailParam.put("inspCode", e62.getScfmx40());

                recordDetailParam.put("inspName", e62.getScfmx41());
                recordDetailParam.put("inspQty", null == e62.getNcfmx06() ? defalutNum : e62.getNcfmx06());
                recordDetailParam.put("inspQtyUnit", e62.getScfmx07());
                recordDetailParam.put("inspPrice", e62.getNcfmx08());
                recordDetailParam.put("inspTimes", e62.getNcfmx13());

                recordDetailParam.put("inspDays", null == e62.getNcfmx14() ? defalutNum : e62.getNcfmx14());
                recordDetailParam.put("doctorAdvice", e62.getScfmx17());
                recordDetailParam.put("paymentState", e62.getScfmx20());
                recordDetailParam.put("comment", e62.getScfmx36());
                recordDetailParam.put("executeState", e62.getScfmx44());

                recordDetailParam.put("createTime", nowDate);
                recordDetailParam.put("updateTime", nowDate);
                recordDetailParam.put("recordState", Consts.RECORD_STATE_VALID);
                recordDetailParam.put("pid", pid);

                recordDetailParams.add(recordDetailParam);
            }
        });

        map.put("recordDetailParams", recordDetailParams);
        map.put("drugParams", drugParams);
        return map;
    }


    /**
     * 组装用户遗传报告进度表参数
     *
     * @param y02s
     * @return
     */
    private List<Map<String, Object>> makeGeneProgressParam(List<YYGLY02DTO> y02s, Date date) {
        if (CollectionUtils.isEmpty(y02s)) {
            return null;
        }
        String pid = y02s.get(0).getPid();
        List<UserMapping> userMappings = userMappingMapper.getUserMappingByPidAndRecordState(pid, Consts.RECORD_STATE_VALID);
        Map<String, String> userMap = Maps.newHashMap();
        userMappings.stream().forEach(userMapping -> {
            userMap.put(userMapping.getHisId(), userMapping.getUserId());
        });

        List<Map<String, Object>> makeGeneProgressParams = Lists.newArrayListWithCapacity(y02s.size());
        y02s.forEach(yygly02 -> {
            String patientId = yygly02.getPatientid();
            Map<String, Object> param = Maps.newHashMapWithExpectedSize(20);
            param.put("id", ObjectIdUtils.stringId());
            param.put("pid", pid);
            param.put("userId", userMap.get(patientId));
            param.put("hisId", patientId);
            param.put("checkNo", yygly02.getCheckNo());

            param.put("name", yygly02.getName());
            param.put("checkTime", yygly02.getCheckTime());
            param.put("rxCode", yygly02.getScfxx01());
            param.put("doctorName", yygly02.getUsername());
            param.put("deptName", yygly02.getDeptname());

            param.put("lcyx", yygly02.getLcyx());
            param.put("state", yygly02.getState());
            param.put("inspCode", yygly02.getInspCode());
            param.put("inspName", yygly02.getInspName());
            param.put("createTime", date);

            param.put("updateTime", date);
            param.put("recordState", Consts.RECORD_STATE_VALID);

            makeGeneProgressParams.add(param);
        });
        return makeGeneProgressParams;
    }

    /**
     * 检查审核
     */
    void singleInspCheck(List<UserMapping> userMappings, String pid, Date date) {
        LOGGER.info("检查审核: 开始. pid={}", pid);
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<SpInspCheckDTO> callChecks;
        try {
            callChecks = userInspCheckMapper.getSpInspCheckSingle(pid);
            LOGGER.info("检查审核: 执行存储过程sp_insp_check完成,commit. pid={}.", pid);
        } catch (Exception e) {
            LOGGER.error("检查审核: 执行存储过程sp_insp_check异常,rollback. pid={}. ", pid, e);
            return;
        }

        Map<String, List<Map<String, Object>>> checkMap = this.makeUserInspCheckParam(callChecks, date, pid, userMappings);
        List<Map<String, Object>> insertParams = checkMap.get("insertParams");
        List<Map<String, Object>> updateParams = checkMap.get("updateParams");

        if (!CollectionUtils.isEmpty(insertParams) || !CollectionUtils.isEmpty(updateParams)) {
            try {
                if (!CollectionUtils.isEmpty(insertParams)) {
                    userInspCheckMapper.batchInsertInspCheck(insertParams);
                }
                if (!CollectionUtils.isEmpty(updateParams)) {
                    userInspCheckMapper.batchUpdatetInspCheck(updateParams);
                }
                LOGGER.info("检查审核: 更新完成,commit. pid={}, insert={}, update={}.", pid, insertParams.size(), updateParams.size());
            } catch (Exception e) {
                LOGGER.error("检查审核: 更新失败,rollback. pid={}. ", pid, e);
            }
        }

        stopwatch.stop();
        LOGGER.info("检查审核: 结束. pid={},cost:{} mills  ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }


    private Map<String, List<Map<String, Object>>> makeUserInspCheckParam(List<SpInspCheckDTO> checks, Date date, String pid, List<UserMapping> userMappings) {
        Map<String, List<Map<String, Object>>> maps = Maps.newHashMap();
        if (CollectionUtils.isEmpty(checks)) {
            LOGGER.info("检查审核: 入参为空. pid:{}", pid);
            return maps;
        }

        String hospitalId = StringUtils.EMPTY;
        if (!CollectionUtils.isEmpty(userMappings)) {
            hospitalId = userMappings.get(0).getHospitalId();
        }

        List<Map<String, Object>> insertParams = Lists.newArrayListWithCapacity(checks.size());
        List<Map<String, Object>> updateParams = Lists.newArrayListWithCapacity(checks.size());

        List<UserInspCheck> inspChecks = Lists.newArrayListWithCapacity(100);

        try {
            inspChecks = userInspCheckMapper.getUserInspCheckByPidAndState(pid, Consts.RECORD_STATE_VALID);
        } catch (Exception e) {
            LOGGER.error("检查审核: 组装参数时异常. pid:{}", pid, e);
        }

        Map<String, UserInspCheck> checkMaps = Maps.newHashMapWithExpectedSize(inspChecks.size());
        if (!CollectionUtils.isEmpty(inspChecks)) {
            inspChecks.forEach(inspCheck -> {
                String mapKey = inspCheck.getPid() + ":" + inspCheck.getInspCode() + ":" + inspCheck.getGender();
                checkMaps.put(mapKey, inspCheck);
            });
        }

        for (SpInspCheckDTO check : checks) {
            String key = check.pid + ":" + check.inspCode + ":" + check.gender;
            UserInspCheck inspCheck = checkMaps.getOrDefault(key, null);
            if (null == inspCheck) {
                // 新增
                Map<String, Object> insertParam = Maps.newHashMapWithExpectedSize(16);
                insertParam.put("id", ObjectIdUtils.stringId());
                insertParam.put("hospitalId", hospitalId);
                insertParam.put("pid", check.pid);
                insertParam.put("userId", check.userId);
                insertParam.put("inspCode", check.inspCode);

                insertParam.put("gender", check.gender);
                insertParam.put("rxFlag", check.rxFlag);
                insertParam.put("reportFlag", check.reportFlag);
                insertParam.put("inspFlag", check.inspFlag);
                insertParam.put("rxCode", check.rxCode);

                insertParam.put("rxDate", check.rxDate);
                insertParam.put("reportDate", check.reportDate);
                insertParam.put("manualFlag", 0);
                insertParam.put("hospitalFlag", check.hospitalFlag);
                insertParam.put("reportCode", check.reportCode);

                insertParam.put("createTime", date);
                insertParam.put("updateTime", date);
                insertParam.put("recordState", Consts.RECORD_STATE_VALID);

                insertParams.add(insertParam);
            } else {
                // 更新内容: 自动审核的部分;手动勾选无报告，但目前有报告编码的数据
                boolean updateCheck = (null != inspCheck.getManualFlag() && Boolean.FALSE == inspCheck.getManualFlag())
                        || (Consts.NO_BIND_REPORD_CODE.equals(inspCheck.getReportCode()) && StringUtils.isNotEmpty(check.reportCode));
                if (updateCheck) {
                    Map<String, Object> updateParam = Maps.newHashMapWithExpectedSize(10);
                    Date reportDate = inspCheck.getReportDate() == null ? check.reportDate : inspCheck.getReportDate();
                    Integer rxFlag = (Boolean.TRUE.equals(inspCheck.getRxFlag())) ? 1 : check.rxFlag;
                    Integer reportFlag = (Boolean.TRUE.equals(inspCheck.getReportFlag())) ? 1 : check.reportFlag;
                    updateParam.put("userId", check.userId);
                    updateParam.put("rxFlag", rxFlag);
                    updateParam.put("reportFlag", reportFlag);
                    updateParam.put("inspFlag", check.inspFlag);
                    updateParam.put("rxCode", check.rxCode);

                    updateParam.put("rxDate", check.rxDate);
                    updateParam.put("reportDate", reportDate);
                    updateParam.put("hospitalFlag", check.hospitalFlag);
                    updateParam.put("reportCode", check.reportCode);
                    updateParam.put("updateTime", date);

                    updateParam.put("manualFlag", 0);
                    updateParam.put("id", inspCheck.getId());

                    updateParams.add(updateParam);
                }
            }
        }

        maps.put("insertParams", insertParams);
        maps.put("updateParams", updateParams);
        return maps;
    }

    /**
     * 查询用户的所有的lis检验报告和明细,分费用机构查询
     */
    void doctorAllLisReports(UserMapping userMapping, Date nowDate) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String pid = userMapping.getPid();
        String hisId = userMapping.getHisId();
        LOGGER.info("同步用户数据: allLis开始. pid={}, dah={}", pid, hisId);

        List<Map<String, Object>> lisReportList = Lists.newArrayListWithCapacity(50);
        List<Map<String, Object>> lisReportDetailList = Lists.newArrayListWithCapacity(50 * 5);

        Stopwatch makePramStopwatch = Stopwatch.createStarted();
        FYJGS.stream().forEach(fyjg -> {
            try {
                // 组装的参数
                WsHisLisAllReq allReq = this.allLisReq(userMapping, fyjg);
                WsHisLisAllResp allLisResp = xiangyaClient.doctorAllLis(allReq);
                boolean normalResp = null != allLisResp && 1 == allLisResp.getResultstatus();
                if (!normalResp) {
                    String errMsg = String.format("同步用户数据: allLis.返回结果异常。pid=%s, dah=%s", pid, hisId);
                    LOGGER.error("{},allLisResp={}", errMsg, JSON.toJSONString(allLisResp));
                    throw new RuntimeException(errMsg);
                }

                // 组装lis和lisdetail参数
                Map<String, List<Map<String, Object>>> lisParamMap = this.allLisReportParam(allLisResp.getLisAllResult(), userMapping, nowDate);
                if (!CollectionUtils.isEmpty(lisParamMap)) {
                    List<Map<String, Object>> lisReportMaps = lisParamMap.get("lisReportMaps");
                    List<Map<String, Object>> lisReportDetailMaps = lisParamMap.get("lisReportDetailMaps");
                    lisReportList.addAll(lisReportMaps);
                    lisReportDetailList.addAll(lisReportDetailMaps);
                }
            } catch (Exception e) {
                String errMsg = String.format("同步用户数据: allLis.请求异常。 pid=%s, dah=%s", pid, hisId);
                LOGGER.error(errMsg, e);
                throw new RuntimeException(errMsg, e);
            }
        });
        makePramStopwatch.stop();
        LOGGER.info("同步用户数据: 请求并组装allLis相关参数. pid={},dah={}. cost={} mills", pid, hisId, makePramStopwatch.elapsed(TimeUnit.MILLISECONDS));

        try {
            // 删除并新增 InspLisReport
            long countDoctorLisReport = doctorInspListMapper.countByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.LIS.getReportType()), hisId, pid);
            if (countDoctorLisReport > 0) {
                doctorInspListMapper.deleteByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.LIS.getReportType()), hisId, pid);
            }
            if (!CollectionUtils.isEmpty(lisReportList)) {
                doctorInspListMapper.batchInsertDoctorInspectList(lisReportList);
            }

            // 删除并更新
            long countDoctorLisDetail = doctorInspLisDetailMapper.countByUserCodeAndPid(hisId, pid);
            if (countDoctorLisDetail > 0) {
                doctorInspLisDetailMapper.deleteByUserCodeAndPid(hisId, pid);
            }
            if (!CollectionUtils.isEmpty(lisReportDetailList)) {
                doctorInspLisDetailMapper.batchInsertDoctorInspectLisDetail(lisReportDetailList);
            }
            LOGGER.info("同步用户数据: allLis,更新完成,commit. pid={},hisId={}. lis: delete={}, add={}; lisDetail: delete={}, add={}.", pid, hisId, countDoctorLisReport, lisReportList.size(), countDoctorLisDetail, lisReportDetailList.size());
        } catch (Exception e) {
            LOGGER.error("同步用户数据: allLis. 更新失败,rollback. pid={},his={}. ", pid, hisId, e);
            throw new RuntimeException(String.format("更新用户lis异常,pid=%s,his=%s", pid, hisId), e);
        }

        stopwatch.stop();
        LOGGER.info("同步用户数据: allLis完成. pid={},hisId={},cost:{} mills ", pid, hisId, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 组装lis相关数据
     *
     * @return
     */
    private Map<String, List<Map<String, Object>>> allLisReportParam(WsHisLisAllResult lisAllResult, UserMapping userMapping, Date nowDate) {
        String pid = userMapping.getPid();
        String dah = userMapping.getHisId();
        if (null == lisAllResult || null == lisAllResult.getResultlist() || CollectionUtils.isEmpty(lisAllResult.getResultlist().getResultRows())) {
            LOGGER.info("同步用户数据:allLis. 返回值为空。pid={},dah={}", pid, dah);
            return null;
        }

        // lis下所有的报告
        List<ResultRow> resultRows = lisAllResult.getResultlist().getResultRows();
        // lis报告参数
        List<Map<String, Object>> lisReportMaps = Lists.newArrayListWithCapacity(resultRows.size());
        // lis明细报告参数
        List<Map<String, Object>> lisReportDetailMaps = Lists.newArrayListWithCapacity(resultRows.size() * 5);


        resultRows.forEach(resultRow -> {
            Map<String, Object> reportMap = Maps.newHashMapWithExpectedSize(30);
            reportMap.put("id", ObjectIdUtils.stringId());
            reportMap.put("hospitalId", userMapping.getHospitalId());
            reportMap.put("reportCode", resultRow.getBgdh());
            reportMap.put("reportName", resultRow.getXmmc());
            reportMap.put("reportType", Integer.parseInt(ReportTypeEnum.LIS.getReportType()));

            // row.getCfh()
            reportMap.put("rxCode", resultRow.getCfh());
            // row.getCfmxh()
            reportMap.put("rxDetlCode", StringUtils.EMPTY);
            reportMap.put("pid", pid);
            reportMap.put("userCode", dah);
            reportMap.put("userExtCode", userMapping.getDhisId());

            reportMap.put("userName", resultRow.getXm());
            reportMap.put("userAge", resultRow.getNl());
            reportMap.put("sampleCode", StringUtils.EMPTY);
            reportMap.put("sampleName", StringUtils.EMPTY);
            String sampleDate = resultRow.getCyrq();
            reportMap.put("sampleDate", StringUtils.isEmpty(sampleDate) ? null : sampleDate);

            String inspDate = null == resultRow.getSjrq() ? null : resultRow.getSjrq();
            reportMap.put("inspDate", inspDate);
            String reportDate = StringUtils.isEmpty(resultRow.getBgrq()) ? null : resultRow.getBgrq();
            reportMap.put("reportDate", reportDate);
            reportMap.put("diagnosis", StringUtils.isEmpty(resultRow.getLczd()) ? StringUtils.EMPTY : resultRow.getLczd());
            // 开方医生 row.getKfys()
            reportMap.put("rxDr", StringUtils.EMPTY);
            reportMap.put("rxDeptCode", StringUtils.isEmpty(resultRow.getKdks()) ? StringUtils.EMPTY : resultRow.getKdks());

            reportMap.put("reportDr", resultRow.getBgys());
            reportMap.put("reviewDr", resultRow.getShys());
            reportMap.put("comment", StringUtils.isEmpty(resultRow.getBz()) ? StringUtils.EMPTY : resultRow.getBz());
            reportMap.put("createTime", nowDate);
            reportMap.put("updateTime", nowDate);

            reportMap.put("recordState", Consts.RECORD_STATE_VALID);
            // 标本种类
            reportMap.put("specimenType", StringUtils.EMPTY);
            reportMap.put("fileUrl", "");
            reportMap.put("fileName", "");

            lisReportMaps.add(reportMap);

            if (null == resultRow.getRows() || CollectionUtils.isEmpty(resultRow.getRows().getRowList())) {
                return;
            }
            // lis报告明细
            List<AppReportLISRow> rowDetails = resultRow.getRows().getRowList();
            rowDetails.forEach(rowDetail -> {
                Map<String, Object> detailParam = Maps.newHashMapWithExpectedSize(20);
                detailParam.put("id", ObjectIdUtils.stringId());
                detailParam.put("hospitalId", userMapping.getHospitalId());
                detailParam.put("reportCode", resultRow.getBgdh());
                detailParam.put("rxCode", resultRow.getCfh());
                detailParam.put("rxDetlCode", StringUtils.EMPTY);

                detailParam.put("pid", userMapping.getPid());
                detailParam.put("userCode", userMapping.getHisId());
                detailParam.put("userExtCode", userMapping.getDhisId());
                detailParam.put("inspCode", rowDetail.getXmid());
                detailParam.put("inspName", rowDetail.getJyxm());

                detailParam.put("inspSname", rowDetail.getXmjc());
                detailParam.put("inspValue", rowDetail.getJyz());
                detailParam.put("inspUnit", rowDetail.getJyzdw());
                detailParam.put("inspState", StringUtils.isEmpty(rowDetail.getJyzzt()) ? null : rowDetail.getJyzzt());
                detailParam.put("inspRenge", rowDetail.getCkqj());

                detailParam.put("createTime", nowDate);
                detailParam.put("updateTime", nowDate);
                detailParam.put("recordState", Consts.RECORD_STATE_VALID);

                lisReportDetailMaps.add(detailParam);
            });

        });


        Map<String, List<Map<String, Object>>> lisParamMap = Maps.newHashMap();
        lisParamMap.put("lisReportMaps", lisReportMaps);
        lisParamMap.put("lisReportDetailMaps", lisReportDetailMaps);

        return lisParamMap;
    }

    /**
     * 查询用户的pacs检验报告和明细
     */
    void doctorPacsReports(UserMapping userMapping, Date nowDate) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String pid = userMapping.getPid();
        String hisId = userMapping.getHisId();
        LOGGER.info("同步用户数据: pacs开始，参数pid={},hisId={}", pid, hisId);
        List<AppReportRow> rows = this.doctorReportRows(userMapping, ReportTypeEnum.PACS);
        if (null == rows) {
            LOGGER.info("同步用户数据: pacs列表,no pacs data. pid={},hisId={}", pid, hisId);
            return;
        }
        int size = rows.size() > 10 ? rows.size() : 10;
        // 报告列表参数
        List<Map<String, Object>> pacsReportParams = Lists.newArrayListWithCapacity(size);
        // pacs明细列表
        List<Map<String, Object>> pacsDetailParams = Lists.newArrayListWithCapacity(size);
        rows.forEach(row -> {
            // pacs检查明细结果
            WsHisReportPACSReq pacsReq = this.doctorPacsDetailReq(userMapping, row);
            WsAppReportPACSResp pacsResp;
            try {
                pacsResp = xiangyaClient.doctorPacs(pacsReq);
            } catch (Exception e) {
                LOGGER.error("同步用户数据: pacs明细. 异常。lisReq={}", JSON.toJSONString(pacsReq));
                throw new RuntimeException("同步用户数据: pacs明细. 异常。", e);
            }

            boolean normalResp = null != pacsResp && 1 == pacsResp.getResultstatus();
            if (!normalResp) {
                String errMsg = String.format("同步用户数据: pacs明细，返回结果异常。pid=%s, hisId=%s", pid, hisId);
                LOGGER.error("{},pacsDetailResp={}", errMsg, JSON.toJSONString(pacsResp));
                throw new RuntimeException(errMsg);
            }
            Map<String, Object> parcReportParam = this.doctorPacsReportParam(pacsResp.getResult(), row, userMapping, nowDate);
            pacsReportParams.add(parcReportParam);
            Map<String, Object> detailParam = this.doctorPacsDetailParam(row, pacsResp, userMapping, nowDate);
            if (null != detailParam && detailParam.size() > 0) {
                pacsDetailParams.add(detailParam);
            }
        });

        try {
            // 删除并新增 InspLisReport
            long countDoctorPacsReport = doctorInspListMapper.countByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.PACS.getReportType()), hisId, pid);
            if (countDoctorPacsReport > 0) {
                doctorInspListMapper.deleteByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.PACS.getReportType()), hisId, pid);
            }
            if (!CollectionUtils.isEmpty(pacsReportParams)) {
                doctorInspListMapper.batchInsertDoctorInspectList(pacsReportParams);
            }
            long countDoctorPacsDetail = doctorInspPacsDetailMapper.countByUserCodeAndPid(hisId, pid);
            if (countDoctorPacsDetail > 0) {
                doctorInspPacsDetailMapper.deleteByUserCodeAndPid(hisId, pid);
            }
            if (!CollectionUtils.isEmpty(pacsDetailParams)) {
                doctorInspPacsDetailMapper.batchInsertDoctorInspectPacsDetail(pacsDetailParams);
            }
            LOGGER.info("同步用户数据: pacs，更新完成,commit. pid={},hisId={}. pacs: delete={},add={}; pacsDetail: delete={},add={}", pid, hisId, countDoctorPacsReport, pacsReportParams.size(), countDoctorPacsDetail, pacsDetailParams.size());
        } catch (Exception e) {
            LOGGER.error("同步用户数据: pacs，更新失败,rollback. pid={},his={}", pid, hisId, e);
            throw new RuntimeException(String.format("更新用户pacs异常,pid=%s,his=%s", pid, hisId), e);
        }

        stopwatch.stop();
        LOGGER.info("同步用户数据: pacs完成. pid={},hisId={},cost:{} mills ", pid, hisId, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 查询用户的遗传
     */
    void doctorGeneReports(UserMapping userMapping, Date nowDate) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String pid = userMapping.getPid();
        String hisId = userMapping.getHisId();
        LOGGER.info("同步用户数据: gens开始. pid={},hisId={}", pid, hisId);
        List<AppReportRow> rows = this.doctorReportRows(userMapping, ReportTypeEnum.GENE);
        if (null == rows) {
            LOGGER.info("同步用户数据: gens. no gens data. pid={},hisId={}", pid, hisId);
            return;
        }
        int size = rows.size() > 10 ? rows.size() : 10;

        // 报告列表参数
        List<Map<String, Object>> doctorGeneReportParams = Lists.newArrayListWithCapacity(size);
        rows.forEach(row -> {
            Map<String, Object> parcReportParam = this.doctorGensReportParam(row, userMapping, nowDate);
            doctorGeneReportParams.add(parcReportParam);
        });

        try {
            // 删除并新增 InspLisReport
            long countDoctorGeneReport = doctorInspListMapper.countByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.GENE.getReportType()), hisId, pid);
            if (countDoctorGeneReport > 0) {
                doctorInspListMapper.deleteByReportTypeAndUserCodeAndPid(Integer.parseInt(ReportTypeEnum.GENE.getReportType()), hisId, pid);

            }
            if (!CollectionUtils.isEmpty(doctorGeneReportParams)) {
                doctorInspListMapper.batchInsertDoctorInspectList(doctorGeneReportParams);
            }
            LOGGER.info("同步用户数据: gens,更新完成,commit. pid={},hisId={},delete={},add={}", pid, hisId, countDoctorGeneReport, doctorGeneReportParams.size());
        } catch (Exception e) {
            LOGGER.error("同步用户数据: gens,更新失败,rollback. pid={},hisId={}", pid, hisId, e);
            throw new RuntimeException(String.format("同步用户数据: gens,更新失败. pid=%s,his=%s", pid, hisId), e);
        }
        stopwatch.stop();
        LOGGER.info("同步用户数据: gene完成,pid={},hisId={},cost:{} mills ", pid, hisId, stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 请求列表返回参数
     */
    private List<AppReportRow> doctorReportRows(UserMapping userMapping, ReportTypeEnum reportTypeEnum) throws
            Exception {
        WsHisReportReq appReportReq = this.doctorReportReq(userMapping, reportTypeEnum);
        // appReportReq为空时会抛出异常,因为是单独服务模块封装的,所以需要单独处理该情况
        if (appReportReq != null) {
            WsAppReportResp reportResp = xiangyaClient.doctorReports(appReportReq);
            // 判断返回数据是否异常
            boolean normalResp = null != reportResp && 1 == reportResp.getResultstatus();
            if (!normalResp) {
                String errMsg = null == reportResp ? "返回数据为空" : reportResp.getErrormsg();
                LOGGER.error("患者pid={},hidId={},报告类型={}，返回报告数据异常。reportResp={}", userMapping.getPid(), userMapping.getHisId(), reportTypeEnum.getReportType(), JSON.toJSONString(reportResp));
                throw new RuntimeException(errMsg);
            }
            boolean rowListResult = Optional.of(reportResp).map(WsAppReportResp::getResult)
                    .map(AppReportResult::getRows)
                    .map(AppReportRows::getRowList).isPresent();
            if (!rowListResult) {
                LOGGER.info("患者pid={},hisId={},报告类型={}，数据为空", userMapping.getPid(), userMapping.getHisId(), reportTypeEnum.getReportType());
                return new ArrayList();
            }
            return reportResp.getResult().getRows().getRowList();
        }
        return null;
    }

    /**
     * 组装pacs Report列表数据
     *
     * @param row
     * @param pacsResult
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> doctorPacsReportParam(AppReportPACSResult pacsResult, AppReportRow
            row, UserMapping userMapping, Date nowDate) {
        Map<String, Object> param = Maps.newHashMapWithExpectedSize(30);
        param.put("id", ObjectIdUtils.stringId());
        param.put("hospitalId", userMapping.getHospitalId());
        param.put("reportCode", row.getBgdh());
        param.put("reportName", row.getDjms());
        param.put("reportType", Integer.parseInt(ReportTypeEnum.PACS.getReportType()));

        param.put("rxCode", row.getCfh());
        param.put("rxDetlCode", row.getCfmxh());
        param.put("pid", userMapping.getPid());
        param.put("userCode", userMapping.getHisId());
        param.put("userExtCode", userMapping.getDhisId());

        param.put("userName", row.getXm());
        param.put("userAge", row.getNl());
        param.put("sampleCode", null);
        param.put("sampleName", null);
        param.put("sampleDate", null);

        param.put("inspDate", null);
        String reportDate = StringUtils.isEmpty(row.getBgrq()) ? row.getKfrq() : row.getBgrq();
        param.put("reportDate", StringUtils.isEmpty(reportDate) ? null : reportDate);
        param.put("diagnosis", pacsResult.getLczd());
        param.put("rxDr", row.getKfys());
        param.put("rxDeptCode", pacsResult.getKdks());

        param.put("reportDr", row.getBgys());
        param.put("reviewDr", row.getShys());
        param.put("comment", null == pacsResult.getBz());
        param.put("createTime", nowDate);
        param.put("updateTime", nowDate);

        param.put("recordState", Consts.RECORD_STATE_VALID);
        param.put("specimenType", null);
        param.put("fileUrl", row.getBaseurl());
        param.put("fileName", row.getFilename());

        return param;
    }

    /**
     * 组装 gens Report列表数据
     *
     * @param row
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> doctorGensReportParam(AppReportRow row, UserMapping userMapping, Date
            nowDate) {
        Map<String, Object> param = Maps.newHashMapWithExpectedSize(30);
        param.put("id", ObjectIdUtils.stringId());
        param.put("hospitalId", userMapping.getHospitalId());
        param.put("reportCode", row.getBgdh());
        param.put("reportName", row.getDjms());
        param.put("reportType", Integer.parseInt(ReportTypeEnum.GENE.getReportType()));

        param.put("rxCode", row.getCfh());
        param.put("rxDetlCode", row.getCfmxh());
        param.put("pid", userMapping.getPid());
        param.put("userCode", userMapping.getHisId());
        param.put("userExtCode", userMapping.getDhisId());

        param.put("userName", row.getXm());
        param.put("userAge", row.getNl());
        param.put("sampleCode", row.getBblxid());
        param.put("sampleName", row.getBblx());
        param.put("sampleDate", null);

        param.put("inspDate", null);
        String reportDate = StringUtils.isEmpty(row.getBgrq()) ? row.getKfrq() : row.getBgrq();
        param.put("reportDate", StringUtils.isEmpty(reportDate) ? null : reportDate);
        param.put("diagnosis", null);
        param.put("rxDr", row.getKfys());
        param.put("rxDeptCode", null);

        param.put("reportDr", row.getBgys());
        param.put("reviewDr", row.getShys());
        param.put("comment", null);
        param.put("createTime", nowDate);
        param.put("updateTime", nowDate);

        param.put("recordState", Consts.RecordState.VALID.value());
        param.put("specimenType", null);
        param.put("fileUrl", row.getBaseurl());
        param.put("fileName", row.getFilename());

        return param;
    }

    /**
     * 组装 t_insp_pacs_detail 参数
     *
     * @param row
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> doctorPacsDetailParam(AppReportRow row, WsAppReportPACSResp
            pacsResp, UserMapping userMapping, Date nowDate) {
        AppReportPACSResult result = pacsResp.getResult();
        Map<String, Object> pacsParam = Maps.newHashMapWithExpectedSize(20);
        pacsParam.put("id", ObjectIdUtils.stringId());
        pacsParam.put("hospitalId", userMapping.getHospitalId());
        pacsParam.put("reportCode", row.getBgdh());
        pacsParam.put("rxCode", row.getCfh());
        pacsParam.put("rxDetlCode", row.getCfmxh());

        pacsParam.put("pid", userMapping.getPid());
        pacsParam.put("userCode", userMapping.getHisId());
        pacsParam.put("userExtCode", userMapping.getDhisId());
        pacsParam.put("inspCode", null);
        pacsParam.put("inspName", null == result ? "" : result.getJcxm());

        pacsParam.put("inspDesc", null == result ? "" : result.getBgms());
        pacsParam.put("inspDx", null == result ? "" : result.getBgjg());
        pacsParam.put("createTime", nowDate);
        pacsParam.put("updateTime", nowDate);
        pacsParam.put("recordState", Consts.RECORD_STATE_VALID);

        String reportDate = null == result ? null : result.getBgrq();
        pacsParam.put("reportDate", StringUtils.isEmpty(reportDate) ? null : reportDate);

        return pacsParam;
    }

    /**
     * 组装报告列表请求，分切lis报告列表和pacs报告列表,根据reportType区别
     *
     * @param userMapping
     * @param reportType
     * @return
     */
    private WsHisReportReq doctorReportReq(UserMapping userMapping, ReportTypeEnum reportType) {
        if (null == userMapping || StringUtils.isEmpty(userMapping.getDhisId())) {
            LOGGER.warn("组装报告请求请求失败：userMapping或者dhisId为空");
            return null;
        }

        WsHisReportReq req = new WsHisReportReq();
        req.setWsid(WsidTypeEnum.DOCTOR_REPORTS.getWsid());
        req.setPid(userMapping.getPid());
        req.setDah(userMapping.getHisId());
        req.setCxksrq(DateUtils.NaturalMonthYearAgo());
        req.setCxjsrq(DateUtils.nowDate());
        req.setBglx(reportType.getReportType());
        req.setDybh(XY_WS_DYBH);
        req.setDymm(XY_WS_DYMM);

        return req;
    }

    /**
     * 组装all_lis报告请求
     *
     * @return
     */
    private WsHisLisAllReq allLisReq(UserMapping userMapping, String fyjg) {
        WsHisLisAllReq allReq = new WsHisLisAllReq();
        allReq.setWsid(WsidTypeEnum.DOCTOR_ALL_LIS.getWsid());
        allReq.setPid(userMapping.getPid());
        allReq.setDah(userMapping.getHisId());
        allReq.setCxksrq(DateUtils.NaturalMonthYearAgo());
        allReq.setCxjsrq(DateUtils.nowDate());
        allReq.setBgdh(null);
        allReq.setFyjg(fyjg);
        allReq.setBglx("1");
        allReq.setDybh(XY_WS_DYBH);
        allReq.setDymm(XY_WS_DYMM);

        return allReq;
    }


    /**
     * 组装lis报告详情请求参数
     *
     * @param userMapping
     * @param row
     * @return
     */
    private WsHisReportPACSReq doctorPacsDetailReq(UserMapping userMapping, AppReportRow row) {
        if (null == userMapping || StringUtils.isEmpty(userMapping.getDhisId())) {
            LOGGER.warn("组装pacs报告明细请求请求失败：userMapping或者dhisId为空");
            return null;
        }
        WsHisReportPACSReq req = new WsHisReportPACSReq();
        req.setWsid(WsidTypeEnum.DOCTOR_PACS.getWsid());
        req.setFyjg(row.getFyjg());
        req.setBgdh(row.getBgdh());
        req.setPid(userMapping.getPid());
        req.setDah(userMapping.getHisId());
        req.setDybh(XY_WS_DYBH);
        req.setDymm(XY_WS_DYMM);

        return req;
    }

    /**
     * 组装病例信息
     *
     * @param emrA01s 病例信息
     * @param nowDate 日期
     * @return emrRecordParams
     */
    private List<Map<String, Object>> makeEmrRecordParams(List<ZxxyEmrA01> emrA01s, Date nowDate) {
        List<String> emrCodes = Lists.newArrayListWithCapacity(emrA01s.size());
        List<String> userCodes = Lists.newArrayListWithCapacity(emrA01s.size());
        emrA01s.forEach(emrA01 -> {
            emrCodes.add(emrA01.getSwdfl01());
            userCodes.add(emrA01.getSbljl10());
        });

        List<EmrDic> emrDics = Collections.EMPTY_LIST;
        List<SyncHisUser> hisUsers = Collections.EMPTY_LIST;

        if (!CollectionUtils.isEmpty(emrCodes)) {
            emrDics = emrDicMapper.getEmrDicsInEmrCodes(emrCodes);
        }
        if (!CollectionUtils.isEmpty(hisUsers)) {
            hisUsers = syncHisUserMapper.getSyncHisUserInUserCodes(userCodes);
        }

        Map<String, String> emrDicMap = Maps.newHashMapWithExpectedSize(emrDics.size());
        Map<String, String> hisUserMap = Maps.newHashMapWithExpectedSize(emrDics.size());
        emrDics.forEach(emrDic -> emrDicMap.put(emrDic.getEmrCode(), emrDic.getEmrName()));
        hisUsers.forEach(hisUser -> hisUserMap.put(hisUser.getUserCode(), hisUser.getUserName()));

        List<Map<String, Object>> emrRecordParams = Lists.newArrayListWithCapacity(emrA01s.size());
        emrA01s.forEach(emrA01 -> {
            Map<String, Object> param = Maps.newHashMapWithExpectedSize(12);

            param.put("id", ObjectIdUtils.stringId());
            param.put("emrCode", emrA01.getSwdfl01());
            param.put("emrName", emrDicMap.get(emrA01.getSwdfl01()));
            param.put("pid", emrA01.getPid());
            param.put("userCode", emrA01.getSzybh01());
            param.put("doctorCode", emrA01.getSbljl10());
            param.put("doctorName", hisUserMap.get(emrA01.getSbljl10()));
            param.put("recordTime", emrA01.getDbljl06() != null ? emrA01.getDbljl06() : emrA01.getDbljl07());
            param.put("recordType", emrA01.getSbljl11() != null ? emrA01.getSbljl11() : 999);
            param.put("emrSort", emrA01.getSbljl01() != null ? emrA01.getSbljl01() : 999);
            param.put("createTime", nowDate);
            param.put("updateTime", nowDate);
            param.put("recordState", emrA01.getSbljl20());

            emrRecordParams.add(param);
        });

        return emrRecordParams;
    }

    /**
     * 组装病例明细信息
     *
     * @param emrA02s 病例明细
     * @param nowDate 日期
     * @return emrRecordDetailParams
     */
    private List<Map<String, Object>> makeEmrRecordDetailParams(List<ZxxyEmrA02> emrA02s, Date nowDate) {
        List<String> itemCodes = Lists.newArrayListWithCapacity(emrA02s.size());
        emrA02s.forEach(emrA02 -> itemCodes.add(emrA02.getSblxm01()));

        List<EmrDicDetl> emrDicDetls;
        try {
            emrDicDetls = emrDicDetlMapper.getEmrDicDetlInItemCodes(itemCodes);
        } catch (Exception e) {
            LOGGER.error("同步用户数据: case. 获取病历明细项目字典表失败", e);
            throw new RuntimeException("同步用户数据: case. 获取病历明细项目字典表失败", e);
        }

        Map<String, EmrDicDetl> illnessDetailMap = Maps.newHashMapWithExpectedSize(emrDicDetls.size());
        emrDicDetls.forEach(emrDicDetl -> illnessDetailMap.put(emrDicDetl.getItemCode(), emrDicDetl));

        List<Map<String, Object>> emrRecordDetailParams = Lists.newArrayList();
        emrA02s.forEach(emrA02 -> {
            Map<String, Object> param = Maps.newHashMapWithExpectedSize(14);
            EmrDicDetl illnessDetail = illnessDetailMap.get(emrA02.getSblxm01());
            param.put("id", ObjectIdUtils.stringId());
            param.put("emrCode", emrA02.getSwdfl01());
            param.put("emrSort", emrA02.getSbljl01() != null ? emrA02.getSbljl01() : 999);
            param.put("pid", emrA02.getPid());
            param.put("userCode", emrA02.getSzybh01());
            param.put("itemCode", emrA02.getSblxm01());
            param.put("itemName", null == illnessDetail ? "" : illnessDetail.getItemCode());
            param.put("preDesc", null == illnessDetail ? "" : illnessDetail.getPreDesc());
            param.put("itemContent", emrA02.getSblsj03());
            param.put("sufDesc", null == illnessDetail ? "" : illnessDetail.getSufDesc());
            param.put("itemPcode", null == illnessDetail ? "" : illnessDetail.getItemPcode());
            param.put("createTime", nowDate);
            param.put("updateTime", nowDate);
            param.put("recordState", Consts.RECORD_STATE_VALID);

            emrRecordDetailParams.add(param);
        });

        return emrRecordDetailParams;
    }

    /**
     * 查询用户的lis检验报告和明细
     */
    void lisReports(UserMapping userMapping, Date nowDate) throws Exception {
        String grid = userMapping.getDhisId();
        String pid = userMapping.getPid();
        LOGGER.info("app同步用户信息: lis开始. pid={}, grid={}", pid, grid);
        List<AppReportRow> rows = this.appReportRows(userMapping, ReportTypeEnum.LIS);
        if (null == rows) {
            LOGGER.info("app同步用户信息: lis列表. no lis data. pid={}, grid={}.", pid, grid);
            return;
        }

        int size = rows.size() > 10 ? rows.size() : 10;
        List<Map<String, Object>> reportParams = Lists.newArrayListWithCapacity(size);
        List<Map<String, Object>> reportDetailParams = Lists.newArrayListWithCapacity(size * 10);

        rows.forEach(row -> {
            // 根据row取出 对应的报告单的详情
            WsAppReportLISReq lisReq = this.lisReportReq(userMapping, row);
            WsAppReportLISResp lisDetail;
            try {
                lisDetail = xiangyaClient.getLis(lisReq);
            } catch (Exception e) {
                LOGGER.error("app同步用户信息: lis明细. 异常。pid={}, grid={}. row={}", pid, grid, JSON.toJSONString(row), e);
                throw new RuntimeException(String.format("app同步用户信息: lis明细. 异常。pid={}, grid={}.", pid, grid), e);
            }

            // 返回数据异常
            boolean excepResp = null == lisDetail || 1 != lisDetail.getResultstatus();
            if (excepResp) {
                LOGGER.error("app同步用户信息: lis明细，返回结果为空。pid={}, grid={}", pid, grid);
                return;
            }
            Map<String, Object> param = this.reportParam(row, lisDetail, userMapping, nowDate);
            reportParams.add(param);

            boolean rowListFlag = Optional.of(lisDetail)
                    .map(WsAppReportLISResp::getResult)
                    .map(AppReportLISResult::getRows)
                    .map(AppReportLISRows::getRowList)
                    .isPresent();
            if (!rowListFlag) {
                LOGGER.info("app同步用户信息: lis. 数据为空，pid={},grid={}", pid, grid);
                return;
            }

            List<AppReportLISRow> lisDetailRows = lisDetail.getResult().getRows().getRowList();
            lisDetailRows.forEach(lisDetailRow -> {
                Map<String, Object> detailParam = this.reportDetailParam(lisDetailRow, row, userMapping, nowDate);
                reportDetailParams.add(detailParam);
            });
        });


    }

    /**
     * 请求列表返回参数
     *
     * @return
     */
    private List<AppReportRow> appReportRows(UserMapping userMapping, ReportTypeEnum reportTypeEnum) throws Exception {
        String pid = userMapping.getPid();
        String grid = userMapping.getDhisId();
        LOGGER.info("app同步用户信息: 报告列表. pid={},grid={},reportType={}", pid, grid, reportTypeEnum.getReportType());
        WsAppReportReq appReportReq = this.reportReq(userMapping, reportTypeEnum);
        WsAppReportResp reportResp = null;
        if (appReportReq != null) {
            reportResp = xiangyaClient.getReports(appReportReq);
            LOGGER.info("获取用户:{} 报告列表:{}成功: ", userMapping.getUserId(),reportResp.toString());
        }

        // 判断返回数据是否异常
        boolean excepResp = null == reportResp || 1 != reportResp.getResultstatus();
        if (excepResp) {
            String errMsg = null == reportResp ? "返回数据为空" : reportResp.getErrormsg();
            LOGGER.error("app同步用户信息: 报告列表. pid={},grid={},reportType={}，返回报告数据为空", pid, grid, reportTypeEnum.getReportType());
            throw new RuntimeException(errMsg);
        }
        boolean rowListResult = Optional.of(reportResp).map(WsAppReportResp::getResult)
                .map(AppReportResult::getRows)
                .map(AppReportRows::getRowList).isPresent();
        if (!rowListResult) {
            LOGGER.info("app同步用户信息: 报告列表. pid={},grid={},reportType={}，数据为空", pid, grid, reportTypeEnum.getReportType());
            return new ArrayList();
        }
        return reportResp.getResult().getRows().getRowList();
    }

    /**
     * 组装报告列表请求，lis报告列表和pacs报告列表,根据reportType区别
     *
     * @param userMapping
     * @param reportType
     * @return
     */
    private WsAppReportReq reportReq(UserMapping userMapping, ReportTypeEnum reportType) {
        if (null == userMapping || StringUtils.isBlank(userMapping.getDhisId())) {
            LOGGER.error("app同步用户信息: 报告列表. 报告列表参数异常：userMapping or dhisId is empty");
            return null;
        }
        WsAppReportReq req = new WsAppReportReq();
        req.setWsid(WsidTypeEnum.REPORTS.getWsid());
        req.setPid(userMapping.getPid());
        req.setGrid(userMapping.getDhisId());
        req.setCxksrq(DateUtils.NaturalMonthYearAgo());
        req.setCxjsrq(DateUtils.nowDate());
        req.setBglx(reportType.getReportType());
        req.setDybh(XY_WS_DYBH);
        req.setDymm(XY_WS_DYMM);

        return req;
    }

    /**
     * 组装lis报告详情请求参数
     *
     * @param userMapping
     * @param row
     * @return
     */
    private WsAppReportLISReq lisReportReq(UserMapping userMapping, AppReportRow row) {
        if (null == userMapping || StringUtils.isBlank(userMapping.getDhisId())) {
            LOGGER.error("app同步用户信息: lis明细. lis明细参数异常：userMapping or dhisId is empty");
            return null;
        }
        WsAppReportLISReq req = new WsAppReportLISReq();
        req.setWsid(WsidTypeEnum.LIS.getWsid());
        req.setFyjg(row.getFyjg());
        req.setBgdh(row.getBgdh());
        req.setPid(userMapping.getPid());
        req.setGrid(userMapping.getDhisId());
        req.setDybh(XY_WS_DYBH);
        req.setDymm(XY_WS_DYMM);

        return req;
    }

    /**
     * 组装lis report列表数据
     *
     * @param row
     * @param lisDetail
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> reportParam(AppReportRow row, WsAppReportLISResp lisDetail, UserMapping userMapping, Date nowDate) {
        AppReportLISResult lisResult = null == lisDetail ? null : lisDetail.getResult();

        Map<String, Object> param = Maps.newHashMapWithExpectedSize(30);
        param.put("id", ObjectIdUtils.stringId());
        param.put("hospitalId", userMapping.getHospitalId());
        param.put("reportCode", row.getBgdh());
        param.put("reportName", row.getDjms());
        param.put("reportType", Integer.parseInt(ReportTypeEnum.LIS.getReportType()));

        param.put("rxCode", row.getCfh());
        param.put("rxDetlCode", row.getCfmxh());
        param.put("pid", userMapping.getPid());
        param.put("userCode", userMapping.getHisId());
        param.put("userExtCode", userMapping.getDhisId());

        param.put("userName", row.getXm());
        param.put("userAge", row.getNl());
        param.put("sampleCode", row.getBblxid());
        param.put("sampleName", row.getBblx());
        String sampleDate = null == lisResult ? null : lisResult.getCyrq();
        param.put("sampleDate", StringUtils.isBlank(sampleDate) ? null : sampleDate);

        String inspDate = null == lisResult ? null : lisResult.getSjrq();
        param.put("inspDate", StringUtils.isBlank(inspDate) ? null : inspDate);
        String reportDate = StringUtils.isBlank(row.getBgrq()) ? row.getKfrq() : row.getBgrq();
        param.put("reportDate", StringUtils.isBlank(reportDate) ? null : reportDate);
        param.put("diagnosis", null == lisResult ? null : lisResult.getLczd());
        param.put("rxDr", row.getKfys());
        param.put("rxDeptCode", null == lisResult ? null : lisResult.getKdks());

        param.put("reportDr", row.getBgys());
        param.put("reviewDr", row.getShys());
        param.put("comment", null == lisResult ? null : lisResult.getBz());
        param.put("createTime", nowDate);
        param.put("updateTime", nowDate);

        param.put("recordState", Consts.RecordState.VALID.value());
        param.put("specimenType", null == lisResult ? null : lisResult.getBbzl());
        param.put("fileUrl", row.getBaseurl());
        param.put("fileName", row.getFilename());

        return param;
    }


    /**
     * 组装 t_insp_lis_detail 参数
     *
     * @param lisDetailRow
     * @param row
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> reportDetailParam(AppReportLISRow lisDetailRow, AppReportRow row, UserMapping userMapping, Date nowDate) {
        Map<String, Object> detailParam = Maps.newHashMapWithExpectedSize(20);
        detailParam.put("id", ObjectIdUtils.stringId());
        detailParam.put("hospitalId", userMapping.getHospitalId());
        detailParam.put("reportCode", row.getBgdh());
        detailParam.put("rxCode", row.getCfh());
        detailParam.put("rxDetlCode", row.getCfmxh());

        detailParam.put("pid", userMapping.getPid());
        detailParam.put("userCode", userMapping.getHisId());
        detailParam.put("userExtCode", userMapping.getDhisId());
        detailParam.put("inspCode", lisDetailRow.getXmid());
        detailParam.put("inspName", lisDetailRow.getJyxm());

        detailParam.put("inspSname", lisDetailRow.getXmjc());
        detailParam.put("inspValue", lisDetailRow.getJyz());
        detailParam.put("inspUnit", lisDetailRow.getJyzdw());
        detailParam.put("inspState", StringUtils.isBlank(lisDetailRow.getJyzzt()) ? null : lisDetailRow.getJyzzt());
        detailParam.put("inspRenge", lisDetailRow.getCkqj());

        detailParam.put("createTime", nowDate);
        detailParam.put("updateTime", nowDate);
        detailParam.put("recordState", Consts.RecordState.VALID.value());

        return detailParam;
    }


    /**
     * 查询用户的pacs检验报告和明细
     */
    void pacsReports(UserMapping userMapping, Date nowDate) throws Exception {
        String pid = userMapping.getPid();
        String grid = userMapping.getDhisId();
        LOGGER.info("app同步用户信息: pacs开始，pid={},grid={}", pid, grid);
        List<AppReportRow> rows = this.appReportRows(userMapping, ReportTypeEnum.PACS);
        if (null == rows) {
            LOGGER.info("app同步用户信息: pacs. no pacs data. pid={},grid={}.", pid, grid);
            return;
        }

        int size = rows.size() > 10 ? rows.size() : 10;

        Stopwatch stopwatch = Stopwatch.createStarted();
        // 报告列表参数
        List<Map<String, Object>> pacsReportParams = Lists.newArrayListWithCapacity(size);
        // pacs明细列表
        List<Map<String, Object>> pacsDetailParams = Lists.newArrayListWithCapacity(size);
        rows.forEach(row -> {
            // pacs检查明细请求参数
            WsAppReportPACSReq pacsReq = this.appPacsDetailReq(userMapping, row);
            WsAppReportPACSResp pacsResp;
            try {
                pacsResp = xiangyaClient.getPacs(pacsReq);
            } catch (Exception e) {
                String msg = String.format("app同步用户信息: pacs明细. 异常。pid=%s,grid=%s.", pid, grid);
                LOGGER.error(msg + "lisReq={}", JSON.toJSONString(pacsReq));
                throw new RuntimeException(msg, e);
            }

            // 返回数据异常
            boolean excepResp = null == pacsResp || 1 != pacsResp.getResultstatus();
            if (excepResp) {
                LOGGER.error("app同步用户信息: pacs明细，pid={},grid={},no get pacs data", pid, grid);
                return;
            }

            Map<String, Object> parcReportParam = this.pacsReportParam(pacsResp.getResult(), row, userMapping, nowDate);
            pacsReportParams.add(parcReportParam);

            Map<String, Object> detailParam = this.pacsDetailParam(row, pacsResp, userMapping, nowDate);
            if (null != detailParam && detailParam.size() > 0) {
                pacsDetailParams.add(detailParam);
            }
        });

        try {
            // 删除并新增 InspLisReport
            long countPacsReport = inspListMapper.countByExtCodeAndReportType(grid, Integer.parseInt(ReportTypeEnum.PACS.getReportType()));
            if (countPacsReport > 0) {
                inspListMapper.deleteByExtCodeAndReportType(grid, Integer.parseInt(ReportTypeEnum.PACS.getReportType()));
            }
            if (!CollectionUtils.isEmpty(pacsReportParams)) {
                inspListMapper.batchInsertInspectList(pacsReportParams);
            }
            long countPacsDetail = inspLisDetailMapper.countByExtCode(grid);
            if (countPacsDetail > 0) {
                inspLisDetailMapper.deleteByExtCode(grid);
            }
            if (!CollectionUtils.isEmpty(pacsDetailParams)) {
                inspLisDetailMapper.batchInsertInspectLisDetail(pacsDetailParams);
            }
            LOGGER.info("app同步用户信息: pacs. 更新完成，commit. pid={},grid={},InspLisReport: delete={},add={}. InspPacsDetail: delete={},add={}.", pid, grid, countPacsReport, pacsReportParams.size(), countPacsDetail, pacsDetailParams.size());
        } catch (Exception e) {
            String errMsg = String.format("app同步用户信息: pacs. 更新失败，rollback. pid=%s,grid=%s", pid, grid);
            LOGGER.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        stopwatch.stop();
        LOGGER.info("app同步用户信息: pacs结束. dhisId={},cost:{} mills ", userMapping.getDhisId(), stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    /**
     * 组装pacs报告详情请求参数
     *
     * @param userMapping
     * @param row
     * @return
     */
    private WsAppReportPACSReq appPacsDetailReq(UserMapping userMapping, AppReportRow row) {
        if (null == userMapping || StringUtils.isBlank(userMapping.getDhisId())) {
            LOGGER.error("app同步用户信息: pacs明细. pacs明细参数异常：userMapping or dhisId is empty");
            return null;
        }
        WsAppReportPACSReq req = new WsAppReportPACSReq();
        req.setWsid(WsidTypeEnum.PACS.getWsid());
        req.setFyjg(row.getFyjg());
        req.setBgdh(row.getBgdh());
        req.setPid(userMapping.getPid());
        req.setGrid(userMapping.getDhisId());
        req.setDybh(XY_WS_DYBH);
        req.setDymm(XY_WS_DYMM);

        return req;
    }


    /**
     * 组装pacs Report列表数据
     *
     * @param row
     * @param pacsResult
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> pacsReportParam(AppReportPACSResult pacsResult, AppReportRow row, UserMapping userMapping, Date nowDate) {
        Map<String, Object> param = Maps.newHashMapWithExpectedSize(30);
        param.put("id", ObjectIdUtils.stringId());
        param.put("hospitalId", userMapping.getHospitalId());
        param.put("reportCode", row.getBgdh());
        param.put("reportName", row.getDjms());
        param.put("reportType", Integer.parseInt(ReportTypeEnum.PACS.getReportType()));

        param.put("rxCode", row.getCfh());
        param.put("rxDetlCode", row.getCfmxh());
        param.put("pid", userMapping.getPid());
        param.put("userCode", userMapping.getHisId());
        param.put("userExtCode", userMapping.getDhisId());

        param.put("userName", row.getXm());
        param.put("userAge", row.getNl());
        param.put("sampleCode", row.getBblxid());
        param.put("sampleName", row.getBblx());
        param.put("sampleDate", null);

        param.put("inspDate", null);
        String reportDate = StringUtils.isBlank(row.getBgrq()) ? row.getKfrq() : row.getBgrq();
        param.put("reportDate", StringUtils.isBlank(reportDate) ? null : reportDate);
        param.put("diagnosis", pacsResult.getLczd());
        param.put("rxDr", row.getKfys());
        param.put("rxDeptCode", pacsResult.getKdks());

        param.put("reportDr", row.getBgys());
        param.put("reviewDr", row.getShys());
        param.put("comment", null == pacsResult ? null : pacsResult.getBz());
        param.put("createTime", nowDate);
        param.put("updateTime", nowDate);

        param.put("recordState", Consts.RecordState.VALID.value());
        param.put("specimenType", null);
        param.put("fileUrl", row.getBaseurl());
        param.put("fileName", row.getFilename());

        return param;
    }

    /**
     * 组装 t_insp_pacs_detail 参数
     *
     * @param row
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> pacsDetailParam(AppReportRow row, WsAppReportPACSResp pacsResp, UserMapping userMapping, Date nowDate) {
        AppReportPACSResult result = pacsResp.getResult();
        // 获取对应报告明细
        Map<String, Object> pacsParam = Maps.newHashMapWithExpectedSize(20);
        pacsParam.put("id", ObjectIdUtils.stringId());
        pacsParam.put("hospitalId", userMapping.getHospitalId());
        pacsParam.put("reportCode", row.getBgdh());
        pacsParam.put("rxCode", row.getCfh());
        pacsParam.put("rxDetlCode", row.getCfmxh());

        pacsParam.put("pid", userMapping.getPid());
        pacsParam.put("userCode", userMapping.getHisId());
        pacsParam.put("userExtCode", userMapping.getDhisId());
        pacsParam.put("inspCode", null);
        pacsParam.put("inspName", null == result ? null : result.getJcxm());

        pacsParam.put("inspDesc", null == result ? null : result.getBgms());
        pacsParam.put("inspDx", null == result ? null : result.getBgjg());
        pacsParam.put("createTime", nowDate);
        pacsParam.put("updateTime", nowDate);
        pacsParam.put("recordState", Consts.RecordState.VALID.value());

        String reportDate = null == result ? null : result.getBgrq();
        pacsParam.put("reportDate", StringUtils.isBlank(reportDate) ? null : reportDate);

        return pacsParam;
    }

    /**
     * 查询用户的遗传
     */
    void geneReports(UserMapping userMapping, Date nowDate) throws Exception {
        String pid = userMapping.getPid();
        String grid = userMapping.getDhisId();
        Stopwatch stopwatch = Stopwatch.createStarted();
        LOGGER.info("app同步用户信息: gens开始. pid={},hisId={}", pid, grid);
        List<AppReportRow> rows = this.appReportRows(userMapping, ReportTypeEnum.GENE);
        if (null == rows) {
            LOGGER.info("app同步用户信息: gens. no gens data. pid={},grid={}", pid, grid);
            return;
        }

        int size = rows.size() > 10 ? rows.size() : 10;

        // 报告列表参数
        List<Map<String, Object>> geneReportParams = Lists.newArrayListWithCapacity(size);
        rows.forEach(row -> {
            Map<String, Object> geneReportParam = this.gensReportParam(row, userMapping, nowDate);
            geneReportParams.add(geneReportParam);
        });

        try {
            // 删除并新增 InspLisReport
            long countInspLisReport = inspListMapper.countByExtCodeAndReportType(grid, Integer.parseInt(ReportTypeEnum.GENE.getReportType()));
            if (countInspLisReport > 0) {
                inspListMapper.deleteByExtCodeAndReportType(grid, Integer.parseInt(ReportTypeEnum.GENE.getReportType()));
            }
            inspListMapper.batchInsertInspectList(geneReportParams);
            LOGGER.info("app同步用户信息: gene. grid={}, delete={}, add={}", grid, countInspLisReport, geneReportParams.size());
        } catch (Exception e) {
            String errMsg = String.format("app同步用户信息: gene. 更新失败,rollback. pid={},grid={}", pid, grid);
            LOGGER.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
        stopwatch.stop();
        LOGGER.info("app同步用户信息: gene结束. pid={},grid={},cost:{} mills ", pid, grid, stopwatch.elapsed(TimeUnit.MILLISECONDS));

    }

    /**
     * 组装 gens Report列表数据
     *
     * @param row
     * @param userMapping
     * @param nowDate
     * @return
     */
    private Map<String, Object> gensReportParam(AppReportRow row, UserMapping userMapping, Date nowDate) {
        Map<String, Object> param = Maps.newHashMapWithExpectedSize(30);
        param.put("id", ObjectIdUtils.stringId());
        param.put("hospitalId", userMapping.getHospitalId());
        param.put("reportCode", row.getBgdh());
        param.put("reportName", row.getDjms());
        param.put("reportType", Integer.parseInt(ReportTypeEnum.GENE.getReportType()));

        param.put("rxCode", row.getCfh());
        param.put("rxDetlCode", row.getCfmxh());
        param.put("pid", userMapping.getPid());
        param.put("userCode", userMapping.getHisId());
        param.put("userExtCode", userMapping.getDhisId());

        param.put("userName", row.getXm());
        param.put("userAge", row.getNl());
        param.put("sampleCode", row.getBblxid());
        param.put("sampleName", row.getBblx());
        param.put("sampleDate", null);

        param.put("inspDate", null);
        String reportDate = StringUtils.isBlank(row.getBgrq()) ? row.getKfrq() : row.getBgrq();
        param.put("reportDate", StringUtils.isBlank(reportDate) ? null : reportDate);
        param.put("diagnosis", null);
        param.put("rxDr", row.getKfys());
        param.put("rxDeptCode", null);

        param.put("reportDr", row.getBgys());
        param.put("reviewDr", row.getShys());
        param.put("comment", null);
        param.put("createTime", nowDate);
        param.put("updateTime", nowDate);

        param.put("recordState", Consts.RecordState.VALID.value());
        param.put("specimenType", null);
        param.put("fileUrl", row.getBaseurl());
        param.put("fileName", row.getFilename());

        return param;
    }


}
