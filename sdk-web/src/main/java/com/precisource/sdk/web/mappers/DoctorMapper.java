package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.dto.TeamMemberDTO;
import com.precisource.sdk.web.common.domain.entity.Doctor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DoctorMapper {

    int insertSelective(Doctor record);

    Doctor selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Doctor record);

    List<Doctor> getDoctorsInDoctorCodes(@Param("list") List<String> doctorCodes);

    List<TeamMemberDTO> queryInquiryMembers(String inquiryId);
}