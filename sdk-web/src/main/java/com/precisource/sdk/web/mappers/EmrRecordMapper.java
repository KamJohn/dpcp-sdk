package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.EmrRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EmrRecordMapper {

    int insertSelective(EmrRecord record);

    EmrRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(EmrRecord record);

    long countByPid(String pid);

    void deleteByPid(String pid);

    void batchInsertEmrRecord(@Param("list") List<Map<String, Object>> emrRecordParams);

}