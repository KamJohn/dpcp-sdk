package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.InspList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InspListMapper {

    int addInspList(InspList record);

    InspList selectByPrimaryKey(String id);

    List<InspList> getInspListByPid(String pid);

    List<InspList> getInspListByUserName(String uname);

    int countByPid(String pid);

    int updateByPrimaryKeySelective(InspList record);

    long countByExtCodeAndReportType(@Param("extCode") String extCode, @Param("reportType") int reportType);

    void deleteByExtCodeAndReportType(@Param("extCode") String extCode, @Param("reportType") int reportType);

    void batchInsertInspectList(@Param("list") List<Map<String, Object>> reportParams);

    int deleteByPid(String pid);
}