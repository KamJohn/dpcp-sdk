package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.dto.UserDTO;
import com.precisource.sdk.web.common.domain.entity.UserRefresh;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserRefreshMapper {

    int addUserRefresh(UserRefresh userRefresh);

    int udpateRefresh(UserRefresh userRefresh);

    List<UserDTO> getUserIdsAndPidsByStage(String stage);
}