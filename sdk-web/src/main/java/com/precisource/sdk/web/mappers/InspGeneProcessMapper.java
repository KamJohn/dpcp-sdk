package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.InspGeneProcess;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface InspGeneProcessMapper {
    int insertSelective(InspGeneProcess record);

    int updateByPrimaryKeySelective(InspGeneProcess record);

    long countByPid(String pid);

    void delete(String pid);

    void batchInsertInspGeneProcess(@Param("list") List<Map<String, Object>> geneProgressParams);

}