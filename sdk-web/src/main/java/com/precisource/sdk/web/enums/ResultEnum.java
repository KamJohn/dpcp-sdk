package com.precisource.sdk.web.enums;

/**
 * @author kam   @date 2019-06-12 17:07
 * @desc 返回结果枚举类
 */
public enum ResultEnum {

    /**
     * 服务器异常 500
     */
    SERVER_ERROR(500, "服务器异常,请稍后重试"),

    /**
     * 接口请求成功状态码和提示
     */
    SUCCESS(0, "成功"),

    /**
     * 接口请求失败状态码和提示
     */
    FAIL(1, "失败"),


    /** 功能已关闭 */
    DISABLED(101, "功能已关闭");

    /**
     * 返回状态码
     */
    public int code;

    /**
     * 返回信息
     */
    public String msg;

    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}