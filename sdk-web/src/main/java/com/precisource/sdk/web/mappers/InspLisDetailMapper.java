package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.InspLisDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InspLisDetailMapper {

    int insertSelective(InspLisDetail record);

    int updateByPrimaryKeySelective(InspLisDetail record);

    long countByExtCode(String extCode);

    void deleteByExtCode(String grid);

    void batchInsertInspectLisDetail(@Param("list") List<Map<String, Object>> reportDetailParams);
}