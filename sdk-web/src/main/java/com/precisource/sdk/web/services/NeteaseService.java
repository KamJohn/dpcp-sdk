package com.precisource.sdk.web.services;

import com.alibaba.fastjson.JSON;
import com.precisource.open.netease.TeamAPI;
import com.precisource.open.netease.request.team.KickRequest;
import com.precisource.open.netease.response.NeteaseAPIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-06-26 15:25
 * @description
 */
@Service
public class NeteaseService {

    private final Logger LOGGER = LoggerFactory.getLogger(InquirySyncService.class);

    /**
     * 踢人出群
     *
     * @param tid        群id
     * @param ownerAccid 群主accid
     * @param members    踢出的成员accid
     * @throws Exception
     */
    public void kickout(String tid, String ownerAccid, List<String> members) throws Exception {
        KickRequest kickRequest = KickRequest
                .Builder
                .newBuilder()
                .withTid(tid)
                .withOwner(ownerAccid)
                .withMembers(members)
                .build();
        NeteaseAPIResponse response = TeamAPI.kickOut(kickRequest);
        if (!response.isSuccessful()) {
            LOGGER.error("踢人失败.tid:{} ownerAccid:{} members Accid:{}", tid, ownerAccid, JSON.toJSONString(members));
            throw new Exception(response.getDesc());
        }
        LOGGER.info("踢人成功.tid:{} ownerAccid:{} members Accid:{}", tid, ownerAccid, JSON.toJSONString(members));
    }
}
