package com.precisource.sdk.web.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.time.temporal.ChronoUnit.YEARS;

/**
 * @author xinput
 * @date 2018-07-02 19:00
 */
public class DateUtils {
    /**
     * 返回距离当前时间一年的自己月的起始日期，即跨度总共为13个月
     * 比如当前时间2018-07-03，则返回时间应该2017-07-01
     *
     * @return
     */
    public static String NaturalMonthYearAgo() {
        LocalDate today = LocalDate.now();
        LocalDate previousYear = today.minus(1, YEARS);
        //本月的第一天
        LocalDate firstday = LocalDate.of(previousYear.getYear(), previousYear.getMonth(), 1);

        return firstday.toString();
    }

    /**
     * 返回当前日期
     *
     * @return
     */
    public static String nowDate() {
        return LocalDate.now().toString();
    }

    /**
     * date转localdate
     *
     * @param date
     * @return
     */
    public static LocalDate getLocalDate(Date date) {
        if (null == date) {
            return null;
        }

        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.toLocalDate();
    }
}
