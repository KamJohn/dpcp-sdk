package com.precisource.sdk.web.controllers;

import com.precisource.sdk.web.common.domain.entity.InspList;
import com.precisource.sdk.web.common.domain.entity.Notify;
import com.precisource.sdk.web.common.domain.entity.UserRefresh;
import com.precisource.sdk.web.consts.Consts;
import com.precisource.sdk.web.enums.SyncStatusEnum;
import com.precisource.sdk.web.mappers.InspListMapper;
import com.precisource.sdk.web.mappers.NotifyMapper;
import com.precisource.sdk.web.mappers.UserRefreshMapper;
import com.precisource.sdk.web.utils.ObjectIdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InspController {

    @Autowired
    private InspListMapper inspListMapper;

    @Autowired
    private UserRefreshMapper userRefreshMapper;

    @Autowired
    private NotifyMapper notifyMapper;

    @RequestMapping("c")
    public int c(String pid) {
        InspList inspList = new InspList();
        inspList.setPid(pid);
        inspList.setId(ObjectIdUtils.stringId());
        return inspListMapper.addInspList(inspList);
    }

    @RequestMapping("r")
    public List<InspList> r(String pid) {
        return inspListMapper.getInspListByPid(pid);
    }

    @RequestMapping("r1")
    public List<InspList> getInspListByUserName(String uname) {
        return inspListMapper.getInspListByUserName(uname);
    }

    @RequestMapping("d")
    public int d(String pid) {
        return inspListMapper.deleteByPid(pid);
    }

    @RequestMapping("count")
    public int count(String pid) {
        return inspListMapper.countByPid(pid);
    }

    @RequestMapping("refresh")
    public int d() {
        UserRefresh refresh = new UserRefresh();
        // 创建用户信息同步记录示例并保存到数据库
        refresh.setId(ObjectIdUtils.stringId());
        refresh.setPid("989898");
        refresh.setUserId("999999999");
        refresh.setOperatorType(Consts.UserType.DOCTOR.value());
        refresh.setStatus(SyncStatusEnum.SYNCING.getStatus());
        return userRefreshMapper.addUserRefresh(refresh);
    }

    @RequestMapping("notify")
    public int n() {
        Notify notify = new Notify();
        notify.setId(ObjectIdUtils.stringId());
        notify.setContent("12345678");
        notify.setNotifyType("asd");
        return notifyMapper.addNotify(notify);
    }
}
