package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.RxDrugDetl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface RxDrugDetlMapper {
    int insertSelective(RxDrugDetl record);

    int updateByPrimaryKeySelective(RxDrugDetl record);

    void batchInsertRxDrugDetail(@Param("list") List<Map<String, Object>> params);

    long countRxDrugDetlByPid(String pid);

    void deleteRxDrugDetlByPid(String pid);
}