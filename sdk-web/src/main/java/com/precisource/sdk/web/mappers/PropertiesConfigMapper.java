package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.PropertiesConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PropertiesConfigMapper {

    int insertSelective(PropertiesConfig record);

    int updateByPrimaryKeySelective(PropertiesConfig record);

    PropertiesConfig getConfigByKey(@Param("key") String key, @Param("state") int state);
}