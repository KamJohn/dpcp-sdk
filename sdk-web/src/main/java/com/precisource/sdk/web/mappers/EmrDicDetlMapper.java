package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.EmrDicDetl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmrDicDetlMapper {
    int insertSelective(EmrDicDetl record);

    int updateByPrimaryKeySelective(EmrDicDetl record);

    List<EmrDicDetl> getEmrDicDetlInItemCodes(@Param("list") List<String> itemCodes);
}