package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.RxRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface RxRecordMapper {

    int insertSelective(RxRecord record);

    void batchInsertRxRecord(@Param("list") List<Map<String, Object>> params);

    long countByPid(String pid);

    void deleteByPid(String pid);

    int updateByPrimaryKeySelective(RxRecord record);
}