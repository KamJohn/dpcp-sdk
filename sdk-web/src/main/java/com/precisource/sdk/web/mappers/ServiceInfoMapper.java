package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.ServiceInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ServiceInfoMapper {
    int insertSelective(ServiceInfo record);

    ServiceInfo selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ServiceInfo record);

    List<ServiceInfo> getServiceInfoByUserIdAndTid(@Param("userId") String userId, @Param("tid") String tid, @Param("recordState") int recordState);

}