package com.precisource.sdk.web.utils;


import com.precisource.sdk.web.enums.ResultEnum;

/**
 * @desc: 返回结果工具类
 * @createTime: 2018/11/22 19:00 @author: Kam
 */
public class ReturnUtil {

    /**
     * 状态码
     */
    private int code;

    /**
     * 枚举定义的消息
     */
    private String msg;

    /**
     * 返回数据
     */
    private Object data;

    /**
     * @desc: 部分参数构造函数
     * @param: [code, msg]
     * @createTime: 2018/11/22 19:05 @author: Kam
     */
    private ReturnUtil(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private ReturnUtil(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 只返回成功,不带数据的
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil success() {
        return new ReturnUtil(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg);
    }

    /**
     * 返回成功，带数据的
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil success(String msg) {
        return new ReturnUtil(ResultEnum.SUCCESS.code, msg);
    }

    /**
     * 返回成功，带数据的
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil success(Object data) {
        return new ReturnUtil(ResultEnum.SUCCESS.code, ResultEnum.SUCCESS.msg, data);
    }

    /**
     * 自定义成功消息，并且带数据的(自定义指的是从封装好的枚举里选择)
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil success(ResultEnum status, Object data) {
        return new ReturnUtil(status.code, status.msg, data);
    }

    /**
     * 返回失败，错误消息自己定义(自定义指的是从封装好的枚举里选择)
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil error(ResultEnum status) {
        return new ReturnUtil(status.code, status.msg);
    }

    /**
     * 返回失败，错误消息自己定义
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil error(ResultEnum status, String msg) {
        return new ReturnUtil(status.code, msg);
    }

    /**
     * 返回失败，自定义消息、错误码、数据
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil error(ResultEnum status, String msg, Object data) {
        return new ReturnUtil(status.code, msg, data);
    }

    /**
     * 返回失败，错误消息统一(服务器故障，请稍后重试)
     *
     * @return ResponseWrapper
     */
    public static ReturnUtil error() {
        return new ReturnUtil(ResultEnum.SERVER_ERROR.code, ResultEnum.SERVER_ERROR.msg);
    }

    public static ReturnUtil error(String msg) {
        return new ReturnUtil(ResultEnum.FAIL.code, msg);
    }
}