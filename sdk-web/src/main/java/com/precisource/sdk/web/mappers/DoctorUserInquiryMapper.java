package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.DoctorUserInquiry;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DoctorUserInquiryMapper {

    int insertSelective(DoctorUserInquiry record);

    int updateByPrimaryKeySelective(DoctorUserInquiry record);

    void updateRecordStateByInquiryId(String inquiryId);
}