package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.OpenNeteaseTeam;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OpenNeteaseTeamMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(OpenNeteaseTeam record);

    OpenNeteaseTeam selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OpenNeteaseTeam record);

    OpenNeteaseTeam getOpenNeteaseTeamByOwnerUserId(String ownerUserId);
}