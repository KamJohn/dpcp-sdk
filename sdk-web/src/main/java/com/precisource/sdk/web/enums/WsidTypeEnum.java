package com.precisource.sdk.web.enums;

/**
 * @author xinput
 * @date 2018-07-03 06:23
 */
public enum WsidTypeEnum {
    REPORTS("WS_CZAPP_YJJGLBCX", "已出结果报告单类型-个人查询"),
    LIS("WS_CZAPP_JYDJCX", "lis检验-个人查询"),
    PACS("WS_CZAPP_JCDJCX", "pacs检查-个人查询"),
    DOCTOR_REPORTS("WS_CZHIS_YJJGLBCX", "已出结果报告单类型-医生查询"),
    DOCTOR_LIS("WS_CZHIS_JYDJCX", "lis检验-医生查询"),
    DOCTOR_PACS("WS_CZHIS_JCDJCX", "pacs检查-医生查询"),
    DOCTOR_ALL_LIS("WS_CZHIS_JYDJCX_ALL", "all_lis检验-医生查询");

    private String wsid;
    private String desc;

    WsidTypeEnum(String wsid, String desc) {
        this.wsid = wsid;
        this.desc = desc;
    }

    public String getWsid() {
        return wsid;
    }

    public String getDesc() {
        return desc;
    }
}
