package com.precisource.sdk.web.services;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.precisource.data.clients.XiangYaClient;
import com.precisource.data.model.vo.NetInquiryResult;
import com.precisource.sdk.web.common.domain.dto.TeamMemberDTO;
import com.precisource.sdk.web.common.domain.entity.*;
import com.precisource.sdk.web.consts.Consts;
import com.precisource.sdk.web.controllers.InquiryController;
import com.precisource.sdk.web.mappers.DoctorUserInquiryMapper;
import com.precisource.sdk.web.mappers.OpenNeteaseTeamMapper;
import com.precisource.sdk.web.mappers.ServiceInfoMapper;
import com.precisource.sdk.web.mappers.UserOnlineInquiryMapper;
import com.precisource.sdk.web.utils.CommonUtils;
import com.precisource.sdk.web.utils.ObjectIdUtils;
import com.precisource.sdk.web.utils.PropertiesUtils;
import com.precisource.sdk.web.utils.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class InquirySyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InquirySyncService.class);

    @Autowired
    private UserOnlineInquiryMapper userOnlineInquiryMapper;

    @Autowired
    private OpenNeteaseTeamMapper openNeteaseTeamMapper;

    @Autowired
    private ServiceInfoMapper serviceInfoMapper;

    @Autowired
    private DoctorUserInquiryMapper doctorUserInquiryMapper;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private NeteaseService neteaseService;

    @Autowired
    private UserMappingService userMappingService;

    @Autowired
    private PropertiesConfigService propertiesConfigService;

    private static Logger logger = LoggerFactory.getLogger(InquiryController.class);

    private String makeInquirySyncKey() {
        return "dpcp:inquiry:incremental:sync";
    }

    private static PropertiesUtils propertiesUtil = new PropertiesUtils("api.properties");

    private static final String XY_BASE_URL = propertiesUtil.getStringProperty("xiangya.baseUrl");

    private static final String XY_ACCESS_KEY = propertiesUtil.getStringProperty("xiangya.access.key");

    private static final String XY_ACCESS_SECRET = propertiesUtil.getStringProperty("xiangya.access.secret");

    /**
     * @author  kam   @date 2019-07-19 14:56
     * @desc    网络问诊数据同步
     */
    @Transactional(rollbackFor = Exception.class)
    public void sync() throws Exception {

        this.closeInquiry();

        boolean enableSync = Boolean.parseBoolean(propertiesConfigService.getProperty("inquiry.sync.job.enable", "true"));
        if (!enableSync) {
            return;
        }
        List<NetInquiryResult> inquiryResults;
        try {
            XiangYaClient xiangyaClient = new XiangYaClient(XY_BASE_URL, XY_ACCESS_KEY, XY_ACCESS_SECRET);
            inquiryResults = xiangyaClient.queryAllInquiries();
        } catch (Exception e) {
            logger.error("获取网络问诊记录异常.", e);
            throw new Exception(e);
        }
        try {
            if (!inquiryResults.isEmpty()) {
                List<String> existedRegistCodes = this.getRegistCodes();
                List<NetInquiryResult> newInquiries = inquiryResults.stream()
                        .filter(netInquiryResult -> !existedRegistCodes.contains(netInquiryResult.getRegistCode()))
                        .collect(Collectors.toList());
                //库中已有未开始的但是同步过来的列表中没有的视为过期或者退号，更新状态，不再显示在列表中
                List<String> pulledRegistCodes = inquiryResults.stream()
                        .map(NetInquiryResult::getRegistCode)
                        .collect(Collectors.toList());
                List<String> readyCodes = this.getRegistCodesByState(Consts.RECORD_STATE_VALID);
                List<String> retreatCodes = readyCodes.stream()
                        .filter(existedCode -> !pulledRegistCodes.contains(existedCode))
                        .collect(Collectors.toList());
                if (!retreatCodes.isEmpty()) {
                    int affectedRows = this.updateInquiryState(retreatCodes, Consts.InquiryState.DONE.value(), Consts.InquiryState.WAITING.value());
                    logger.info("更新网络问诊记录状态成功。total updated rows:{} regist codes:{}", affectedRows, JSON.toJSONString(retreatCodes));
                } else {
                    logger.info("本次同步没有需要变更状态的网络问诊记录");
                }
                if (!newInquiries.isEmpty()) {
                    List<String> doctorCodes = CommonUtils.projectField(newInquiries, netInquiryResult -> netInquiryResult.getDoctorCode());
                    List<String> hisIds = CommonUtils.projectField(newInquiries, netInquiryResult -> netInquiryResult.getUserId());
                    List<String> pids = CommonUtils.projectField(newInquiries, netInquiryResult -> netInquiryResult.getPid());
                    List<Doctor> doctors = Collections.emptyList();
                    if (!CollectionUtils.isEmpty(doctorCodes)) {
                        doctors = doctorService.getDoctorsInRefKeys(doctorCodes);
                    }

                    List<UserMapping> hisIdMappings = Collections.emptyList();
                    List<UserMapping> pidMappings = Collections.emptyList();
                    if (!CollectionUtils.isEmpty(hisIds)) {
                        hisIdMappings = userMappingService.findInHisIds(hisIds);
                        if (!CollectionUtils.isEmpty(pids)) {
                            pidMappings = userMappingService.findInPidAndNotInHid(pids, hisIds);
                        }
                    }
                    Map<String, Doctor> doctorMap = Maps.newHashMapWithExpectedSize((int) (doctors.size() / 0.75));
                    doctors.forEach(doctor -> doctorMap.put(doctor.getRefKey(), doctor));
                    Map<String, String> hisIdMappingMap = Maps.newHashMapWithExpectedSize((int) (hisIdMappings.size() / 0.75));
                    hisIdMappings.forEach(userMapping -> hisIdMappingMap.put(userMapping.getHisId(), userMapping.getUserId()));
                    Map<String, String> pidMappingMap = Maps.newHashMapWithExpectedSize((int) (pidMappings.size() / 0.75));
                    pidMappings.forEach(userMapping -> pidMappingMap.put(userMapping.getPid(), userMapping.getUserId()));
                    List<Map<String, Object>> insertParams = Lists.newArrayListWithCapacity(newInquiries.size());
                    Date now = new Date();
                    newInquiries.forEach(netInquiryResult -> {
                        Map<String, Object> map = Maps.newHashMapWithExpectedSize(30);
                        String userId = hisIdMappingMap.get(netInquiryResult.getUserId());
                        if (StringUtils.isBlank(userId)) {
                            userId = pidMappingMap.get(netInquiryResult.getPid());
                        }
                        Doctor doctor = doctorMap.get(netInquiryResult.getDoctorCode());
                        map.put("id", ObjectIdUtils.stringId());
                        map.put("hospitalId", Consts.DEFAULT_HOSPITAL_ID);
                        map.put("doctorId", doctor != null ? doctor.getId() : null);
                        map.put("registCode", netInquiryResult.getRegistCode());
                        map.put("doctorCode", netInquiryResult.getDoctorCode());
                        map.put("hisId", netInquiryResult.getUserId());
                        map.put("pid", netInquiryResult.getPid());
                        map.put("idCard", netInquiryResult.getIdCard());
                        map.put("date", netInquiryResult.getAppointDate());
                        map.put("userId", userId);
                        map.put("name", netInquiryResult.getName());
                        map.put("gender", netInquiryResult.getGender());
                        map.put("phone", netInquiryResult.getPhone());
                        map.put("state", Consts.InquiryState.WAITING.value());
                        map.put("startTime", null);
                        map.put("endTime", null);
                        map.put("createTime", now);
                        map.put("updateTime", now);
                        map.put("recordState", Consts.RecordState.VALID.value());
                        insertParams.add(map);
                    });
                    this.batchInsertInquiry(insertParams);
                    logger.info("插入网络问诊新记录成功。total insert rows:{} regist codes:{}", newInquiries.size(), JSON.toJSONString(newInquiries));
                } else {
                    logger.info("本次同步没有需要新增的网络问诊预约记录");
                }
            }
        } catch (Exception e) {
            logger.error("转换网络问诊记录数据过程异常。", e);
        }
        String syncKey = this.makeInquirySyncKey();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        RedisUtils.executeResult(jedis -> jedis.set(syncKey, format.format(new Date())));
    }

    /**
     * @author  kam   @date 2019-07-19 14:59
     * @desc    获取所有有效挂号单号
     */
    private List<String> getRegistCodes() {
        return userOnlineInquiryMapper.getRegistCodes();
    }

    /**
     * @author  kam   @date 2019-07-19 15:00
     * @desc    根据状态获取所有有效挂号单号
     */
    private List<String> getRegistCodesByState(Integer state) {
        return userOnlineInquiryMapper.getRegistCodesBystate(state);
    }

    private int updateInquiryState(List<String> retreatCodes, int registCode, int state) {
        return userOnlineInquiryMapper.updateInquiryState(retreatCodes, registCode, state);
    }

    private void batchInsertInquiry(List<Map<String, Object>> insertParams) {
        userOnlineInquiryMapper.batchInsertInquiry(insertParams);
    }

    /**
     * @author kam   @date 2019-05-22 16:59
     * @desc 关闭用户为手动关闭的会诊
     */
    private void closeInquiry() {
        List<String> inquiryIds = userOnlineInquiryMapper.getUnclosedInquiryIds();
        if (!inquiryIds.isEmpty()) {
            inquiryIds.forEach(this::endInquiry);
            LOGGER.info("关闭用户会诊成功。inquiryIds:{}", inquiryIds);
        } else {
            LOGGER.info("无可关闭会诊。");
        }
    }

    /**
     * @author  kam   @date 2019-07-19 14:58
     * @desc    结束会诊
     */
    private void endInquiry(String id) {
        UserOnlineInquiry inquiry = userOnlineInquiryMapper.getUserOnlineInquiryById(id);
        if (Consts.InquiryState.PROCESSING.value() == inquiry.getState()) {
            long count = userOnlineInquiryMapper.countByIdAndState(inquiry.getUserId(), inquiry.getDoctorId(), Consts.InquiryState.PROCESSING.value(), inquiry.getId());
            if (count > 0) {
                LOGGER.info("当前该患者在该医生下有其他正在进行的网络预约记录，忽略IM群组移除操作");
            } else {
                List<TeamMemberDTO> inquiryMembers = doctorService.queryInquiryMembers(inquiry.getId());
                List<String> removedAccids = CommonUtils.projectField(inquiryMembers, teamMemberDTO -> teamMemberDTO.accid);
                OpenNeteaseTeam openNeteaseTeam = openNeteaseTeamMapper.getOpenNeteaseTeamByOwnerUserId(inquiry.getUserId());
                if (openNeteaseTeam == null) {
                    LOGGER.info("未找到该用户所属群组:{}", openNeteaseTeam);
                } else {
                    //用户当前所属群组的服务团队下的所有医生accid账号保留
                    List<ServiceInfo> serviceInfos = serviceInfoMapper.getServiceInfoByUserIdAndTid(inquiry.getUserId(), openNeteaseTeam.getTid(), Consts.RECORD_STATE_VALID);
                    List<String> ignoreMembers = CommonUtils.projectField(serviceInfos, serviceInfo -> serviceInfo.getAccid());
                    removedAccids.removeAll(ignoreMembers);
                    try {
                        neteaseService.kickout(openNeteaseTeam.getTid(), openNeteaseTeam.getOwnerAccid(), removedAccids);
                    } catch (Exception e) {
                        LOGGER.error("结束会诊过程移除群组成员失败.userId:{} doctorId:{}", inquiry.getUserId(), inquiry.getDoctorId(), e);
                        LOGGER.error("结束会诊过程移除IM群组成员失败");
                    }
                }
                doctorUserInquiryMapper.updateRecordStateByInquiryId(inquiry.getId());
                inquiry.setState(Consts.InquiryState.DONE.value());
                inquiry.setEndTime(new Date());
                inquiry.setUpdateTime(inquiry.getEndTime());
                userOnlineInquiryMapper.updateUserOnlineInquiryById(inquiry);
            }
        }
    }
}
