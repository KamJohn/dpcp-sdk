package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.UserMapping;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMappingMapper {
    int insertSelective(UserMapping record);

    int updateByPrimaryKeySelective(UserMapping record);

    List<UserMapping> getUserMappingByPidAndRecordState(@Param("pid") String pid, @Param("state") int recordState);

    List<UserMapping> findInHisIds(@Param("list") List<String> hisIds);

    List<UserMapping> findInPidAndNotInHid(@Param("pids") List<String> pids, @Param("hisIds") List<String> hisIds);

    List<UserMapping> getListByPidAndState(@Param("pid") String pid, @Param("state") int state);
}