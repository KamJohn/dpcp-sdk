package com.precisource.sdk.web.services;

import com.precisource.sdk.web.common.domain.dto.TeamMemberDTO;
import com.precisource.sdk.web.common.domain.entity.Doctor;
import com.precisource.sdk.web.mappers.DoctorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {
    @Autowired
    DoctorMapper doctorMapper;

    public List<Doctor> getDoctorsInRefKeys(List<String> doctorCodes) {
        return doctorMapper.getDoctorsInDoctorCodes(doctorCodes);
    }

    public List<TeamMemberDTO> queryInquiryMembers(String id) {
        return doctorMapper.queryInquiryMembers(id);
    }


}
