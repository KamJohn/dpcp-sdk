package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.EmrDic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmrDicMapper {
    int insertSelective(EmrDic record);

    int updateByPrimaryKeySelective(EmrDic record);

    List<EmrDic> getEmrDicsInEmrCodes(@Param("list") List<String> emrCodes);
}