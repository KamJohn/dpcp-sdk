package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.RxInspDetl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface RxInspDetlMapper {

    int insertSelective(RxInspDetl record);

    int updateByPrimaryKeySelective(RxInspDetl record);

    long countRxRecordDetailByPid(String pid);

    void batchInsertRecordDetail(@Param("list") List<Map<String, Object>> params);

    void deleteRxRecordDetailByPid(String pid);
}