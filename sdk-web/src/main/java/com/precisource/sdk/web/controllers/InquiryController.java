package com.precisource.sdk.web.controllers;

import com.precisource.sdk.web.services.InquirySyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author kam  @date 2019-07-19 14:46
 * @desc 处理网络问诊相关请求
 */
@RestController
@RequestMapping("dpcp/inquiry")
public class InquiryController {

    private static Logger LOGGER = LoggerFactory.getLogger(InquiryController.class);

    @Autowired
    InquirySyncService inquirySyncService;

    @GetMapping("cync")
    public void sync() {
        try {
            inquirySyncService.sync();
        } catch (Exception e) {
            LOGGER.error("网络问诊数据同步异常:{}", e.getMessage());
        }
    }

}
