package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.Notify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface NotifyMapper {

    int addNotify(Notify record);

    int addMessageNotify(com.precisource.dpcp.models.message.Notify record);

    int updateByPrimaryKeySelective(Notify record);

    Notify findBySenderIdAndTargetIdAndNotifyType(@Param("senderId") String senderId, @Param("targetId") String targetId, @Param("notifyType") String notifyType, @Param("targetType") String targetType, @Param("recordState") int recordStateValid);
}