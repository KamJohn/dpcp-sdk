package com.precisource.sdk.web.enums;

/**
 * @author xinput
 * @date 2018-07-10 21:45
 */
public enum SyncStatusEnum {
    SUCCESS(0, "success", "同步成功"),
    SYNCING(1, "syncing", "同步中"),
    FAILURE(2, "failure", "同步失败"),
    SYNCED(3, "synced", "在指定时间内同步过数据");

    private Integer status;
    private String msg;
    private String desc;

    SyncStatusEnum(Integer status, String msg, String desc) {
        this.status = status;
        this.msg = msg;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getDesc() {
        return desc;
    }
}
