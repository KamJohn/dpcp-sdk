package com.precisource.sdk.web.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.*;
import redis.clients.util.Pool;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.precisource.sdk.web.consts.RedisConsts.*;


/**
 * @author <a href="mailto:fivesmallq@gmail.com">fivesmallq</a>
 * @version Revision: 1.0
 * @date 16/8/13 下午4:36
 */
public class RedisUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisUtils.class);
    private static Pool<Jedis> pool = null;
    private static JedisCluster jedisCluster = null;
    private static final Set<HostAndPort> jedisClusterNodes = new HashSet<>();

    static PropertiesUtils redisProperties = new PropertiesUtils("redis.properties");

    private static final String mode = redisProperties.getStringProperty(REDIS_MODE);


    /**
     * 将key 的值设为value ，当且仅当key不存在，等效于 SETNX
     */
    public static final String NX = "NX";

    /**
     * 以秒为单位设置key的过期时间
     */
    public static final String EX = "EX";

    /**
     * 调用set后的返回值
     */
    public static final String OK = "OK";

    private static void init() {
        Integer timeout = redisProperties.getIntProperty(REDIS_TIMEOUT);
        String password = redisProperties.getStringProperty(REDIS_PWD);
        Integer port = redisProperties.getIntProperty(REDIS_PORT);
        JedisPoolConfig poolConfig = getPoolConfig();
        if (mode.equals(MODE_SHARDED)) {
            LOGGER.error(MODE_SHARDED + " Not implemented");
        } else if (mode.equals(MODE_SENTINEL)) {
            String master = redisProperties.getStringProperty(REDIS_MASTER);
            String[] stnArray = redisProperties.getStringArrayProperty(REDIS_SENTINELS);
            Set<String> stns = new HashSet<>();
            for (String stn : stnArray) {
                stns.add(stn);
            }
            pool = new JedisSentinelPool(master, stns, poolConfig, timeout, password);
        } else if (mode.equals(MODE_CLUSTER)) {
            String[] ipsArray = redisProperties.getStringArrayProperty("redis.ips");
            for (String ip : ipsArray) {
                jedisClusterNodes.add(new HostAndPort(ip, port));
            }
            jedisCluster = new JedisCluster(jedisClusterNodes, timeout, poolConfig);
        } else {
            String ip = redisProperties.getStringProperty(REDIS_IP);
            pool = new JedisPool(poolConfig, ip, port, timeout, password);
        }
    }

    public static JedisPoolConfig getPoolConfig() {
        JedisPoolConfig jedisCfg = new JedisPoolConfig();
        Properties poolConfig = redisProperties.getPropertyGroup(REDIS_POOL, true);
        ReflectUtils reflect = ReflectUtils.on(jedisCfg);
        Method[] methods = JedisPoolConfig.class.getMethods();
        for (int i = 0; i < methods.length; i++) {
            Method method = methods[i];
            String methodName = method.getName();
            if (methodName.startsWith("set")) {
                String redisProperties = methodName.replaceFirst("set", "");
                redisProperties = Character.toLowerCase(redisProperties.charAt(0)) + redisProperties.substring(1);
                Object value = poolConfig.get(redisProperties);
                if (value != null) {
                    LOGGER.info("set redisProperties success:{} --> {}", redisProperties, value);
                    reflect
                            .on(jedisCfg)
                            .call(methodName, TypeUtils.cast(value, method.getParameterTypes()[0]));
                }
            }
        }
        return jedisCfg;
    }

    private static Jedis getJedis() {
        return getPool().getResource();
    }

    private static Jedis getJedis(int index) {
        Jedis jedis = getJedis();
        jedis.select(index);
        return jedis;
    }

    public static void execute(Consumer<JedisCommands> consumer) {
        execute(consumer, 0);
    }

    public static void execute(Consumer<JedisCommands> consumer, int index) {
        Jedis jedis = null;
        try {
            if (mode.equals(MODE_CLUSTER)) {
                JedisCluster jedisCluster = getJedisCluster();
                consumer.accept(jedisCluster);
            } else {
                jedis = getJedis(index);
                consumer.accept(jedis);
            }
        } catch (Exception e) {
            LOGGER.error("consumer accept error! consumer:{} index:{}", consumer, index, e);
        } finally {
            if (!mode.equals(MODE_CLUSTER)) {
                closeJedis(jedis);
            }
        }
    }

    public static <R> R executeResult(Function<JedisCommands, R> function) {
        return executeResult(function, 0);
    }

    public static <R> R executeResult(Function<JedisCommands, R> function, int index) {
        R apply = null;
        Jedis jedis = null;
        try {
            if (mode.equals(MODE_CLUSTER)) {
                JedisCluster jedisCluster = getJedisCluster();
                apply = function.apply(jedisCluster);
            } else {
                jedis = getJedis(index);
                apply = function.apply(jedis);
            }
        } catch (Exception e) {
            LOGGER.error("function apply error! consumer:{} index:{}", function, index, e);
        } finally {
            if (!mode.equals(MODE_CLUSTER)) {
                closeJedis(jedis);
            }
        }
        return apply;
    }

    private static void closeJedis(Jedis jedis) {
        if (jedis != null) jedis.close();
    }

    public static Pool<Jedis> getPool() {
        if (pool == null) {
            init();
        }
        return pool;
    }

    public static JedisCluster getJedisCluster() {
        if (jedisCluster == null) {
            init();
        }
        return jedisCluster;
    }
}
