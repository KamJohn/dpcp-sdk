package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.ConsultNotify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ConsultNotifyMapper {
    int addConsultNotify(ConsultNotify record);

    int addNotifyToConsultNotify(com.precisource.dpcp.models.message.Notify record);

    int updateByPrimaryKeySelective(ConsultNotify record);

    ConsultNotify findConsultNotifyByCase(@Param("senderId") String senderId, @Param("targetId") String targetId, @Param("notifyType") String notifyType, @Param("targetType") String targetType, @Param("recordState") int recordState);
}