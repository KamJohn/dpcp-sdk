package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.dto.SpInspCheckDTO;
import com.precisource.sdk.web.common.domain.entity.UserInspCheck;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserInspCheckMapper {
    int insertSelective(UserInspCheck record);

    UserInspCheck selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserInspCheck record);

    List<UserInspCheck> getUserInspCheckByPidAndState(@Param("pid") String pid, @Param("state") int recordStateValid);

    void batchInsertInspCheck(@Param("list") List<Map<String, Object>> insertParams);

    void batchUpdatetInspCheck(@Param("list") List<Map<String, Object>> updateParams);

    List<SpInspCheckDTO> getSpInspCheckSingle(String pid);

    List<SpInspCheckDTO> getSpInspCheckBatch(@Param("map") Map<String, Object> param);

    List<SpInspCheckDTO> getSpInspCheckNew(String pid);
}