package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.EmrRecordDetl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EmrRecordDetlMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(EmrRecordDetl record);

    int updateByPrimaryKeySelective(EmrRecordDetl record);

    long countByPid(String pid);

    void deletByPid(String pid);

    void batchInsertEmrRecordDetail(@Param("list") List<Map<String, Object>> emrRecordDetailParams);

}