package com.precisource.sdk.web;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class})
@PropertySource({"classpath:/api.properties"})
@MapperScan(basePackages = {"com.precisource.sdk.web.mappers"})
@ComponentScan(basePackages = {"com.precisource.sdk.web.services", "com.precisource.sdk.web.controllers"})
public class SdkWebApp {

    public static void main(String[] args) {
        SpringApplication.run(SdkWebApp.class);
    }
}