package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.UserOnlineInquiry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface UserOnlineInquiryMapper {
    int deleteByPrimaryKey(String id);

    int insertSelective(UserOnlineInquiry record);

    UserOnlineInquiry getUserOnlineInquiryById(String id);

    int updateUserOnlineInquiryById(UserOnlineInquiry record);

    List<String> getRegistCodes();

    List<String> getRegistCodesBystate(Integer state);

    int updateInquiryState(@Param("list") List<String> retreatCodes, @Param("registCode") int registCode, @Param("state") int state);

    int test(@Param("params") HashMap<String, Object> params);

    int test2(@Param("list") List<Map<String, Object>> params);

    void batchInsertInquiry(@Param("list") List<Map<String, Object>> insertParams);

    long countByIdAndState(@Param("userId") String userId, @Param("doctorId") String doctorId, @Param("state") int state, @Param("id") String id);

    List<String> getUnclosedInquiryIds();

}