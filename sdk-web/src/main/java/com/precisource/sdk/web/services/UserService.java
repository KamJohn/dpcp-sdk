package com.precisource.sdk.web.services;

import com.google.common.base.Stopwatch;
import com.precisource.sdk.web.common.domain.dto.UserDTO;
import com.precisource.sdk.web.common.domain.entity.UserBasic;
import com.precisource.sdk.web.common.domain.entity.UserMapping;
import com.precisource.sdk.web.common.domain.entity.UserRefresh;
import com.precisource.sdk.web.consts.Consts;
import com.precisource.sdk.web.enums.SyncStatusEnum;
import com.precisource.sdk.web.mappers.UserBasicMapper;
import com.precisource.sdk.web.mappers.UserMappingMapper;
import com.precisource.sdk.web.mappers.UserRefreshMapper;
import com.precisource.sdk.web.utils.ObjectIdUtils;
import com.precisource.sdk.web.utils.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.precisource.sdk.web.utils.PropertiesUtils.propertiesUtil;

@Service
public class UserService {

    @Autowired
    private UserRefreshMapper userRefreshMapper;

    @Autowired
    private UserMappingMapper userMappingMapper;

    @Autowired
    private SdkApiService sdkApiService;

    @Autowired
    private UserBasicMapper userBasicMapper;

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    private Integer WS_KEY_TIME = Integer.valueOf(propertiesUtil.getStringProperty("ws.key.time"));

    private Integer SYNC_KEY_TIME = Integer.valueOf(propertiesUtil.getStringProperty("case.order.key.time"));

    private static final Integer CASE_AND_ORDER_KEY = Integer.valueOf(propertiesUtil.getStringProperty("case.order.key.time"));

    private static ExecutorService executorService = Executors.newFixedThreadPool(20);

    public static AtomicInteger count = new AtomicInteger(0);

    /**
     * @author kam   @date 2019-05-08 15:34
     * @desc 同步用户报告信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void syncAllUserInfo() {
        logger.info("定时任务开始:{}", LocalDateTime.now().toString());
        List<UserDTO> userIdsAndPids = this.getUserIdsAndPidsByStage(Consts.UserStage.CLINIC.value());
        if (CollectionUtils.isEmpty(userIdsAndPids)) {
            logger.info("{}阶段没有数据需要同步", Consts.UserStage.CLINIC.value());
            return;
        }
        logger.info("用户数量：" + userIdsAndPids.size());
        for (UserDTO dto : userIdsAndPids) {
            if (StringUtils.isEmpty(dto.pid)) {
                logger.info("该用户:{}没有对应的pid", dto.id);
                continue;
            }

            String userRefreshInfoKey = this.makeUserRefreshKeyByDoctor(dto.pid);
            String result = RedisUtils.executeResult(jedis -> jedis.set(userRefreshInfoKey, "true", RedisUtils.NX, RedisUtils.EX, 900));
            if (RedisUtils.OK.equalsIgnoreCase(result)) {
                logger.info("设置用户同步数据缓存信息-医生查看，{}={}", userRefreshInfoKey, result);
                executorService.submit(() -> {
                            try {
                                this.refreshUserInfo(dto.pid, dto.id);
                            } catch (Exception e) {
                                logger.error("自动同步用户数据结束，同步用户院内数据异常。pid={},userid={}", dto.pid, dto.id);
                            } finally {
                                RedisUtils.execute(jedis -> jedis.del(userRefreshInfoKey));
                                logger.info("自动同步用户数据结束，同步用户院内数据完成。清除redis key。redis key:{}", userRefreshInfoKey);
                            }
                        }
                );
            }
        }
    }

    private String makeUserRefreshKeyByDoctor(String pid) {
        return "dpcp:user:pid:" + pid + ":doctor";
    }

    /**
     * @param pid, userId [ 用户PID、用户ID ]
     * @author kam   @date 2019-07-19 15:08
     * @desc 医生端: 同步用户院内数据
     */
    private void refreshUserInfo(String pid, String userId) {
        if (StringUtils.isEmpty(pid) || StringUtils.isEmpty(userId)) {
            logger.warn("同步用户数据: pid or userId is null,pid={},userId={}", pid, userId);
            return;
        }
        logger.info("同步用户数据同步用户数据:院内检查信息. pid:{},userId:{}", pid, userId);

        // 创建用户信息同步记录示例并保存到数据库
        UserRefresh refresh = new UserRefresh();
        refresh.setId(ObjectIdUtils.stringId());
        refresh.setPid(pid);
        refresh.setUserId(userId);
        refresh.setOperatorType(Consts.UserType.DOCTOR.value());
        refresh.setStatus(SyncStatusEnum.SYNCING.getStatus());
        userRefreshMapper.addUserRefresh(refresh);

        Stopwatch stopwatch = Stopwatch.createStarted();
        Date nowDate = new Date();

        // 同步开单信息和病例信息,检查报告信息
        List<UserMapping> userMappings = userMappingMapper.getUserMappingByPidAndRecordState(pid, Consts.RECORD_STATE_VALID);
        if (CollectionUtils.isEmpty(userMappings)) {
            logger.error("同步用户数据: pid={},userid={},userMappings is null", pid, userId);
            refresh.setStatus(SyncStatusEnum.FAILURE.getStatus());
            refresh.setMsg("pid=" + pid + ",userMappings is null");
            userRefreshMapper.udpateRefresh(refresh);
            return;
        }
        String wsKey = this.refreshWsKey(pid, Consts.UserType.DOCTOR);
        try {
            String result = RedisUtils.executeResult(jedis -> jedis.set(wsKey, "true", RedisUtils.NX, RedisUtils.EX, WS_KEY_TIME));
            if (RedisUtils.OK.equalsIgnoreCase(result)) {
                logger.info("同步用户数据: 设置ws锁. pid={}, caseAndOrderKey={}", pid, wsKey);
                sdkApiService.syncCaseByPid(pid, nowDate);
                sdkApiService.syncOrderByPid(pid, nowDate);
                sdkApiService.syncGeneReportByPid(pid, nowDate);
                for (UserMapping mapping : userMappings) {
                    sdkApiService.doctorAllLisReports(mapping, nowDate);
                    sdkApiService.doctorPacsReports(mapping, nowDate);
                    sdkApiService.doctorGeneReports(mapping, nowDate);
                }
                sdkApiService.singleInspCheck(userMappings, pid, nowDate);
            } else {
                refresh.setMsg(String.format("同步用户数据: 同步时间间隔过短. pid=%s在%s秒内已经同步过一次，key=%s", pid, WS_KEY_TIME + "", wsKey));
                refresh.setStatus(SyncStatusEnum.SUCCESS.getStatus());
                logger.info("同步用户数据: 同步时间间隔过短. pid={}在{}秒之内已经同步过一次，key={}", pid, WS_KEY_TIME, wsKey);
            }
        } catch (Exception e) {
            logger.error("同步用户数据: 院内检查信息. 用户报告信息异常,pid={}", pid, e);
            // 如果同步失败了，删除这个同步锁可以，否则，30分钟后才能继续同步
            RedisUtils.execute(jedis -> jedis.del(wsKey));
            logger.info("同步用户数据: del wsKey，key={}", wsKey);
            refresh.setMsg("同步用户数据: 用户报告信息异常。" + e.getMessage());
            refresh.setStatus(SyncStatusEnum.FAILURE.getStatus());
        }

        if (StringUtils.isEmpty(refresh.getMsg())) {
            refresh.setStatus(SyncStatusEnum.SUCCESS.getStatus());
            // 存入redis中，用于判断10分钟内已同步
            String syncKey = this.userLastUpdateKey(userId);
            RedisUtils.executeResult(jedis -> jedis.set(syncKey, System.currentTimeMillis() + "", RedisUtils.NX, RedisUtils.EX, SYNC_KEY_TIME));
        }
        stopwatch.stop();
        logger.info("同步用户数据: 院内检查信息. pid={},cost:{} mills ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
        userRefreshMapper.udpateRefresh(refresh);
    }


    /**
     * @author kam  @date 2019-06-14 12:02
     * @desc 患者端: 刷新用户院内报告相关数据
     */
    @Transactional(rollbackFor = Exception.class)
    public void refreshUserInfo(String userId) {
        if (StringUtils.isBlank(userId)) {
            logger.info("刷新用户院内报告相关数据时用户ID不能为空");
            return;
        }
        // Validate userBaisc
        UserBasic userBasic = userBasicMapper.getUserBasicByFlagAndUserId(Consts.RegistFlag.Regist.getRegFlag(), userId);
        if (userBasic == null || StringUtils.isBlank(userBasic.getPid())) {
            logger.error("app同步用户信息: 没有该用户信息或该用户无对应的pid。userId={}", userId);
            return;
        }

        String pid = userBasic.getPid();
        logger.info("app同步用户信息: case and order开始. userId={},pid={}", userId, pid);
        UserRefresh refresh = this.getUserRefresh(pid, userId, Consts.UserType.USER);
        userRefreshMapper.addUserRefresh(refresh);

        Stopwatch stopwatch = Stopwatch.createStarted();
        Date nowDate = new Date();

        // 同步开单信息和病例信息
        String caseAndOrderKey = this.refreshCaseAndOrderKey(userBasic.getPid());
        try {
            String caseAndOrderKeyResult = RedisUtils.executeResult(jedis -> jedis.set(caseAndOrderKey, String.valueOf(System.currentTimeMillis()), RedisUtils.NX, RedisUtils.EX, CASE_AND_ORDER_KEY));
            if (RedisUtils.OK.equalsIgnoreCase(caseAndOrderKeyResult)) {
                logger.info("app同步用户信息: 设置用户同步病例缓存信息，{}={}", caseAndOrderKey, caseAndOrderKeyResult);
                sdkApiService.syncCaseByPid(userBasic.getPid(), nowDate);
                sdkApiService.syncOrderByPid(userBasic.getPid(), nowDate);
            } else {
                refresh.setMsg(String.format("同步用户信息-app，case and order,pid=%s在%s秒内已经同步过一次，key=%s", pid, CASE_AND_ORDER_KEY + "", caseAndOrderKey));
                refresh.setStatus(SyncStatusEnum.SUCCESS.getStatus());
                logger.info("app同步用户信息: case and order,pid={}在{}秒内已经同步过一次，key={}", pid, CASE_AND_ORDER_KEY, caseAndOrderKey);
            }
        } catch (Exception e) {
            logger.error("app同步用户信息: 开单信息和病例信息异常,pid={}", userBasic.getPid(), e);
            // 如果同步失败了，删除这个同步锁，否则，10分钟后才能继续同步
            RedisUtils.execute(jedis -> jedis.del(caseAndOrderKey));
            logger.info("app同步用户信息: 删除用户同步病例缓存信息，key={}", caseAndOrderKey);
            refresh.setMsg("app同步用户信息: 开单信息和病例信息异常:" + e.getMessage());
            refresh.setStatus(SyncStatusEnum.FAILURE.getStatus());
        }

        stopwatch.stop();
        logger.info("app同步用户信息: case and order完成. userId={},pid={},cost:{} mills ", userId, pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));

        List<UserMapping> userMappings = userMappingMapper.getListByPidAndState(pid, Consts.RecordState.VALID.value());
        if (null == userMappings || userMappings.size() == 0) {
            logger.error("app同步用户信息: pid={},userid={},userMappings is null", pid, userId);
            refresh.setStatus(SyncStatusEnum.FAILURE.getStatus());
            refresh.setMsg("pid=" + pid + ",userMappings is null");
            userRefreshMapper.udpateRefresh(refresh);
            return;
        }

        stopwatch.reset().start();
        String wsKey = refreshWsKey(userBasic.getPid(), Consts.UserType.USER);
        logger.info("app同步用户信息: ws开始. pid={}  ", pid);
        try {
            String wsKeyResult = RedisUtils.executeResult(jedis -> jedis.set(wsKey, "true", RedisUtils.NX, RedisUtils.EX, WS_KEY_TIME));
            if (RedisUtils.OK.equalsIgnoreCase(wsKeyResult)) {
                logger.info("app同步用户信息: 设置ws锁. pid={}, caseAndOrderKey={}", pid, wsKey);
                for (UserMapping mapping : userMappings) {
                    sdkApiService.lisReports(mapping, nowDate);
                    sdkApiService.pacsReports(mapping, nowDate);
                    sdkApiService.geneReports(mapping, nowDate);
                }
            } else {
                refresh.setMsg(String.format("同步用户信息-app: ws. pid=%s在%s秒内已经同步过一次，key=%s", pid, WS_KEY_TIME + "", wsKey));
                refresh.setStatus(SyncStatusEnum.SUCCESS.getStatus());
                logger.info("app同步用户信息: ws. pid={}在{}秒之内已经同步过一次，key={}", pid, WS_KEY_TIME, wsKey);
            }
        } catch (Exception e) {
            logger.error("app同步用户信息: 用户检查报告信息异常,pid={}", userBasic.getPid(), e);
            // 如果同步失败了，删除这个同步锁可以，否则，30分钟后才能继续同步
            RedisUtils.execute(jedis -> jedis.del(wsKey));
            logger.info("app同步用户信息: 删除用户同步报告缓存信息-用户查看，key={}", wsKey);
            refresh.setMsg("app同步用户信息: 用户检查报告信息异常:" + e.getMessage());
            refresh.setStatus(SyncStatusEnum.FAILURE.getStatus());
        }

        if (StringUtils.isEmpty(refresh.getMsg())) {
            refresh.setStatus(SyncStatusEnum.SUCCESS.getStatus());
        }
        stopwatch.stop();
        logger.info("app同步用户信息: ws结束. pid={},cost:{} mills ", pid, stopwatch.elapsed(TimeUnit.MILLISECONDS));
        userRefreshMapper.udpateRefresh(refresh);
    }

    private UserRefresh getUserRefresh(String pid, String userId, Consts.UserType userType) {
        UserRefresh refresh = new UserRefresh();
        refresh.setId(ObjectIdUtils.stringId());
        refresh.setPid(pid);
        refresh.setUserId(userId);
        refresh.setOperatorType(userType.value());
        refresh.setStatus(SyncStatusEnum.SYNCING.getStatus());
        return refresh;
    }

    /**
     * 获取用户病例信息和开单信息时存入redis的key
     *
     * @param pid
     * @return
     */
    private String refreshCaseAndOrderKey(String pid) {
        return "dpcp:refresh:case:pid:" + pid;
    }

    /**
     * 同步用户检查报告信息
     *
     * @param pid
     * @return
     */
    private String refreshWsKey(String pid, Consts.UserType userType) {
        return String.format("dpcp:refresh:%s:ws:pid:%s", userType.value(), pid);
    }

    private List<UserDTO> getUserIdsAndPidsByStage(String stage) {
        return userRefreshMapper.getUserIdsAndPidsByStage(stage);
    }

    /**
     * 用户最后更新时间
     *
     * @param userId
     * @return
     */
    private String userLastUpdateKey(String userId) {
        return String.format("dpcp:user:%s:lastUpdate", userId);
    }


}

