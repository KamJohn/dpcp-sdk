package com.precisource.sdk.web.enums;

/**
 * 处方类别
 *
 * @author xinput
 * @date 2018-07-02 16:59
 */
public enum RxTypeEnum {
    RX_TYPE_01("01", "西药"),
    RX_TYPE_02("02", "草药"),
    RX_TYPE_03("03", "项目"),
    RX_TYPE_04("04", "检验"),
    RX_TYPE_05("05", "检查"),
    ;

    private String rxType;
    private String desc;

    RxTypeEnum(String rxType, String desc) {
        this.rxType = rxType;
        this.desc = desc;
    }

    public String getRxType() {
        return rxType;
    }

    public String getDesc() {
        return desc;
    }
}
