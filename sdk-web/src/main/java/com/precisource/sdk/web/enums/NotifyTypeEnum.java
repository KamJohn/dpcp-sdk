//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.precisource.sdk.web.enums;

public enum NotifyTypeEnum {
    ANNOUNCEMENT("announcement", "公告"),
    USER_REMIND_INSPECT("user_remind_inspect", "提醒_用户检查提醒"),
    USER_REMIND_TRANSFER("user_remind_transfer", "提醒_用户团队转移"),
    USER_REMIND_REGISTRATION("user_remind_registration", "提醒_用户挂号就诊"),
    USER_REMIND_SIGN("user_remind_sign", "提醒_用户签约"),
    REMIND_REGISTERED("remind_registered", "提醒_新用户注册"),
    REMIND_REPORT_UPDATE_MALE("remind_report_update_male", "提醒_男方检查报告上传"),
    REMIND_REPORT_UPDATE_FEMALE("remind_report_update_female", "提醒_女方检查报告上传"),
    REMIND_USER_OUT("remind_user_out", "提醒_已就诊通知"),
    REMIND_USER_IN("remind_user_in", "提醒_新加入病人通知"),
    REMIND_TRANSFER("remind_user_transfer", "提醒_手动转移通知"),
    REMIND_DECLINE("remind_transfer_declined", "提醒_转移拒收通知"),
    REMIND_ACCEPT("remind_transfer_accepted", "提醒_转移已被接收通知"),
    IM_PATIENT_TO_DOCTOR("im_patient_to_doctor", "信息_用户到医生"),
    IM_PATIENT_TO_TEAM("im_patient_to_team", "信息_用户到医护团队"),
    IM_DOCTOR_TO_PATIENT("im_doctor_to_patient", "信息_医生到用户"),
    NEW_CONSULT_IM_PATIENT_TO_DOCTOR("new_consult_im_patient_to_doctor", "信息_用户到医生"),
    NEW_CONSULT_USER_ACCESSED("new_consult_user_accessed", "新咨询用户接入");

    private String type;
    private String desc;

    private NotifyTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return this.type;
    }

    public String getDesc() {
        return this.desc;
    }
}
