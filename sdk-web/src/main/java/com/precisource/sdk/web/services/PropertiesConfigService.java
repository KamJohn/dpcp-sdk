package com.precisource.sdk.web.services;

import com.precisource.dpcp.consts.Consts;
import com.precisource.sdk.web.common.domain.entity.PropertiesConfig;
import com.precisource.sdk.web.mappers.PropertiesConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zanxus
 * @date 2018-12-11 15:19
 * @versioin 1.0.0
 * @description
 */
@Service
public class PropertiesConfigService {

    @Autowired
    PropertiesConfigMapper configMapper;

    public String getProperty(String key, String defaultValue) {
        PropertiesConfig config = configMapper.getConfigByKey(key, Consts.RECORD_STATE_VALID);
        if (config != null) {
            return config.getValue();
        }
        return defaultValue;
    }

}
