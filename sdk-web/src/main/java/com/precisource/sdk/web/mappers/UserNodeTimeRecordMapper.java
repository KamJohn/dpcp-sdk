package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.UserNodeTimeRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserNodeTimeRecordMapper {

    int addUserNodeTimeRecord(UserNodeTimeRecord record);

    int updateByPrimaryKeySelective(UserNodeTimeRecord record);

    long countByPid(String pid);

    void deleteByPid(String pid);

}