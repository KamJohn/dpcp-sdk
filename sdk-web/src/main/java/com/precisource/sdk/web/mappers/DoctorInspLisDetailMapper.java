package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.DoctorInspLisDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DoctorInspLisDetailMapper {

    int insertSelective(DoctorInspLisDetail record);

    int updateByPrimaryKeySelective(DoctorInspLisDetail record);

    void deleteByUserCodeAndPid(@Param("hisId") String hisId, @Param("pid") String pid);

    long countByUserCodeAndPid(@Param("hisId") String hisId, @Param("pid") String pid);

    void batchInsertDoctorInspectLisDetail(@Param("list") List<Map<String, Object>> lisReportDetailList);
}