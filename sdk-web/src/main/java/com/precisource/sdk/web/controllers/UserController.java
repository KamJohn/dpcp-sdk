package com.precisource.sdk.web.controllers;

import com.precisource.sdk.web.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kam  @date 2019-05-08 15:35
 * @desc 处理用户相关请求
 */
@RestController
@RequestMapping("dpcp/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("sync/all")
    public void syncAllUserInfo() {
        userService.syncAllUserInfo();
    }

    @GetMapping("refresh/info")
    public void refreshUserInfo(String userId) {
        userService.refreshUserInfo(userId);
    }
}
