package com.precisource.sdk.web.services;

import com.precisource.sdk.web.common.domain.entity.UserMapping;
import com.precisource.sdk.web.mappers.UserMappingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMappingService {

    @Autowired
    UserMappingMapper userMappingMapper;

    public List<UserMapping> findInHisIds(List<String> hisIds) {
        return userMappingMapper.findInHisIds(hisIds);
    }

    public List<UserMapping> findInPidAndNotInHid(List<String> pids, List<String> hisIds) {
        return userMappingMapper.findInPidAndNotInHid(pids, hisIds);
    }
}
