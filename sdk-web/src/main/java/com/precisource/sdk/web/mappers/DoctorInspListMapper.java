package com.precisource.sdk.web.mappers;

import com.precisource.sdk.web.common.domain.entity.DoctorInspList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DoctorInspListMapper {

    int insertSelective(DoctorInspList record);

    int updateByPrimaryKeySelective(DoctorInspList record);

    long countByReportTypeAndUserCodeAndPid(@Param("reportType") int reportType, @Param("hisId") String hisId, @Param("pid") String pid);

    long deleteByReportTypeAndUserCodeAndPid(@Param("reportType") int reportType, @Param("hisId") String hisId, @Param("pid") String pid);

    void batchInsertDoctorInspectList(@Param("list") List<Map<String, Object>> lisReportList);
}