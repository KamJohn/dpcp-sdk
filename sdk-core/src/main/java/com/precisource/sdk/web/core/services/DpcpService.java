package com.precisource.sdk.web.core.services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author  kam  @date 2019-06-12 15:11
 * @desc    接口定义: 和core模块中提供的接口对应
 */
public interface DpcpService {

    /**
     * @author  kam   @date 2019-07-19 11:44
     * @desc    网络问诊数据同步
     */
    @GET("dpcp/inquiry/cync")
    Call<Void> inquirySync();

    /**
     * @author  kam   @date 2019-07-19 11:44
     * @desc    同步用户报告信息
     */
    @GET("dpcp/user/sync/all")
    Call<Void> syncAllUserInfo();

    /**
     * @author  kam   @date 2019-07-19 11:45
     * @desc    患者端: 刷新用户院内报告相关数据
     * @param   userId [ 用户ID ]
     */
    @GET("dpcp/user/refresh/info")
    Call<Void> refreshUserInfo(@Query("userId")String userId);
}
