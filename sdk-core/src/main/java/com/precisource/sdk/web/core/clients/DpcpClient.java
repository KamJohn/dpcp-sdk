package com.precisource.sdk.web.core.clients;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.precisource.sdk.web.core.services.DpcpService;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DpcpClient {
    /**
     * 接口地址前缀
     */
    private String baseUrl;

    /**
     * 接口调用超时时间(s)
     */
    private int httpTimeout;

    /**
     * 成功访问返回状态码
     */
    private static final int SUCCESS = 200;

    /**
     * retrofit
     */
    private Retrofit retrofit;

    private static Logger logger = LoggerFactory.getLogger(DpcpClient.class);

    /**
     * @param baseUrl, httpTimeout [ 接口地址前缀,接口调用超时时间 ]
     * @author kam   @date 2019-06-12 15:22
     * @desc 构造方法，内部执行初始化方法
     */
    public DpcpClient(String baseUrl, int httpTimeout) {
        this.baseUrl = baseUrl;
        this.httpTimeout = httpTimeout;
        this.init();
    }

    /**
     * @author kam   @date 2019-06-12 15:26
     * @desc 初始化方法
     */
    private void init() {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(this.httpTimeout, TimeUnit.SECONDS)
                .readTimeout(this.httpTimeout, TimeUnit.SECONDS)
                .writeTimeout(this.httpTimeout, TimeUnit.SECONDS)
                .build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(this.baseUrl)
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory
                        .create(new GsonBuilder().registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (jsonElement, type, context) ->
                                new Date(jsonElement.getAsJsonPrimitive().getAsLong())).create()));
        this.retrofit = retrofitBuilder
                .build();
    }

    /**
     * @author kam   @date 2019-06-12 15:30
     * @desc 网络问诊数据同步
     */
    public void inquirySync() throws IOException {
        logger.info("网络问诊数据同步调用开始.");
        Response<Void> response = this.retrofit.create(DpcpService.class).inquirySync().execute();
        if (response.code() == SUCCESS) {
            logger.info("网络问诊数据同步调用成功.");
        } else {
            logger.error("网络问诊数据同步调用失败.");
        }
        logger.info("网络问诊数据同步调用结束.");
    }

    /**
     * @author kam   @date 2019-06-12 15:30
     * @desc 同步用户报告信息
     */
    public void syncAllUserInfo() throws IOException {
        logger.info("同步用户报告信息调用开始.");
        Response<Void> response = this.retrofit.create(DpcpService.class).syncAllUserInfo().execute();
        if (response.code() == SUCCESS) {
            logger.info("同步用户报告信息调用成功.");
        } else {
            logger.error("同步用户报告信息用失败.");
        }
        logger.info("同步用户报告信息调用结束.");
    }

    /**
     * @author kam   @date 2019-06-12 15:30
     * @desc 患者端: 刷新用户院内报告相关数据
     */
    public void refreshUserInfo(String userId) throws IOException {
        logger.info("刷新用户院内报告相关数据调用开始.");
        Response<Void> response = this.retrofit.create(DpcpService.class).refreshUserInfo(userId).execute();
        if (response.code() == SUCCESS) {
            logger.info("刷新用户院内报告相关数据调用成功.");
        } else {
            logger.error("刷新用户院内报告相关数据调用失败.");
        }
        logger.info("刷新用户院内报告相关数据调用结束.");
    }
}
