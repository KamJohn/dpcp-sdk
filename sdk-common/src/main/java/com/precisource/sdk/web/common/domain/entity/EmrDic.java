package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_emr_dic
 *
 * @author
 */
public class EmrDic implements Serializable {
    private String id;

    /**
     * 医院ID
     */
    private String hospitalId;

    /**
     * 病历记录编码
     */
    private String emrCode;

    /**
     * 病历记录名称
     */
    private String emrName;

    /**
     * 病历记录父级编码
     */
    private String emrPcode;

    /**
     * 层级
     */
    private Byte level;

    /**
     * 是否末级文档
     */
    private Byte leafFlag;

    /**
     * 是否展示
     */
    private Byte show;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getEmrCode() {
        return emrCode;
    }

    public void setEmrCode(String emrCode) {
        this.emrCode = emrCode;
    }

    public String getEmrName() {
        return emrName;
    }

    public void setEmrName(String emrName) {
        this.emrName = emrName;
    }

    public String getEmrPcode() {
        return emrPcode;
    }

    public void setEmrPcode(String emrPcode) {
        this.emrPcode = emrPcode;
    }

    public Byte getLevel() {
        return level;
    }

    public void setLevel(Byte level) {
        this.level = level;
    }

    public Byte getLeafFlag() {
        return leafFlag;
    }

    public void setLeafFlag(Byte leafFlag) {
        this.leafFlag = leafFlag;
    }

    public Byte getShow() {
        return show;
    }

    public void setShow(Byte show) {
        this.show = show;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}