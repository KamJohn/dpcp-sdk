package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_user_insp_check
 *
 * @author
 */
public class UserInspCheck implements Serializable {
    private String id;

    private String hospitalId;

    /**
     * pid
     */
    private String pid;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 审核项目id
     */
    private String inspCode;

    /**
     * 性别
     */
    private String gender;

    /**
     * 是否开单：0未开；1已开
     */
    private Boolean rxFlag;

    /**
     * 报告单状态：0未出；1已出
     */
    private Boolean reportFlag;

    /**
     * 是否标记异常：0正常；1异常
     */
    private Boolean excFlag;

    /**
     * 项目类别：1必审、2可选
     */
    private Boolean inspFlag;

    /**
     * 处方号
     */
    private String rxCode;

    /**
     * 开单人
     */
    private String rxDoctor;

    /**
     * 开单时间
     */
    private Date rxDate;

    /**
     * 报告时间
     */
    private Date reportDate;

    /**
     * 审核人
     */
    private String createUser;

    /**
     * 状态
     */
    private String updateUserId;

    /**
     * 更新人
     */
    private String updateUser;

    /**
     * 是否手动标记，0否1是
     */
    private Boolean manualFlag;

    /**
     * 是否医院内部报告，0否1是
     */
    private Boolean hospitalFlag;

    /**
     * 报告单编码: 院内报告对应t_doctor_insp_list.report_code
     */
    private String reportCode;

    /**
     * 审核时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Boolean recordState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInspCode() {
        return inspCode;
    }

    public void setInspCode(String inspCode) {
        this.inspCode = inspCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getRxFlag() {
        return rxFlag;
    }

    public void setRxFlag(Boolean rxFlag) {
        this.rxFlag = rxFlag;
    }

    public Boolean getReportFlag() {
        return reportFlag;
    }

    public void setReportFlag(Boolean reportFlag) {
        this.reportFlag = reportFlag;
    }

    public Boolean getExcFlag() {
        return excFlag;
    }

    public void setExcFlag(Boolean excFlag) {
        this.excFlag = excFlag;
    }

    public Boolean getInspFlag() {
        return inspFlag;
    }

    public void setInspFlag(Boolean inspFlag) {
        this.inspFlag = inspFlag;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getRxDoctor() {
        return rxDoctor;
    }

    public void setRxDoctor(String rxDoctor) {
        this.rxDoctor = rxDoctor;
    }

    public Date getRxDate() {
        return rxDate;
    }

    public void setRxDate(Date rxDate) {
        this.rxDate = rxDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Boolean getManualFlag() {
        return manualFlag;
    }

    public void setManualFlag(Boolean manualFlag) {
        this.manualFlag = manualFlag;
    }

    public Boolean getHospitalFlag() {
        return hospitalFlag;
    }

    public void setHospitalFlag(Boolean hospitalFlag) {
        this.hospitalFlag = hospitalFlag;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;

}