package com.precisource.sdk.web.common.domain.dto;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2018-06-19 20:13
 * @description
 */
public class TeamMemberDTO {

    public String accid;
    public String doctorId;
    public String teamId;
    public boolean defaultSpeaker;
    public String name;
    public String title;
}
