package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * r_doctor_user_inquiry
 *
 * @author
 */
public class DoctorUserInquiry implements Serializable {
    private String id;

    /**
     * IM群组id
     */
    private String tid;

    /**
     * 网络问诊预约id
     */
    private String userInquiryId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户accid
     */
    private String userAccid;

    /**
     * 医生id
     */
    private String doctorId;

    /**
     * 医生accid
     */
    private String doctorAccid;

    private Date createTime;

    private Date updateTime;

    private Boolean recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getUserInquiryId() {
        return userInquiryId;
    }

    public void setUserInquiryId(String userInquiryId) {
        this.userInquiryId = userInquiryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAccid() {
        return userAccid;
    }

    public void setUserAccid(String userAccid) {
        this.userAccid = userAccid;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorAccid() {
        return doctorAccid;
    }

    public void setDoctorAccid(String doctorAccid) {
        this.doctorAccid = doctorAccid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }
}