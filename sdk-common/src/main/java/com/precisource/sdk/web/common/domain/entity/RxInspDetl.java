package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * t_rx_insp_detl
 *
 * @author
 */
public class RxInspDetl implements Serializable {
    private String id;

    /**
     * 处方编码
     */
    private String rxCode;

    /**
     * 处方明细编码
     */
    private String rxDetlCode;

    /**
     * 处方类型
     */
    private Byte rxType;

    private String pid;

    /**
     * 项目编码
     */
    private String inspCode;

    /**
     * 项目名称
     */
    private String inspName;

    /**
     * 数量
     */
    private BigDecimal inspQty;

    /**
     * 数量单位
     */
    private String inspQtyUnit;

    /**
     * 单价
     */
    private BigDecimal inspPrice;

    /**
     * 次数
     */
    private Byte inspTimes;

    /**
     * 给药天数
     */
    private Byte inspDays;

    /**
     * 医嘱内容
     */
    private String doctorAdvice;

    /**
     * 缴费状态
     */
    private Byte paymentState;

    /**
     * 备注
     */
    private String comment;

    /**
     * 执行状态
     */
    private Byte executeState;

    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getRxDetlCode() {
        return rxDetlCode;
    }

    public void setRxDetlCode(String rxDetlCode) {
        this.rxDetlCode = rxDetlCode;
    }

    public Byte getRxType() {
        return rxType;
    }

    public void setRxType(Byte rxType) {
        this.rxType = rxType;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getInspCode() {
        return inspCode;
    }

    public void setInspCode(String inspCode) {
        this.inspCode = inspCode;
    }

    public String getInspName() {
        return inspName;
    }

    public void setInspName(String inspName) {
        this.inspName = inspName;
    }

    public BigDecimal getInspQty() {
        return inspQty;
    }

    public void setInspQty(BigDecimal inspQty) {
        this.inspQty = inspQty;
    }

    public String getInspQtyUnit() {
        return inspQtyUnit;
    }

    public void setInspQtyUnit(String inspQtyUnit) {
        this.inspQtyUnit = inspQtyUnit;
    }

    public BigDecimal getInspPrice() {
        return inspPrice;
    }

    public void setInspPrice(BigDecimal inspPrice) {
        this.inspPrice = inspPrice;
    }

    public Byte getInspTimes() {
        return inspTimes;
    }

    public void setInspTimes(Byte inspTimes) {
        this.inspTimes = inspTimes;
    }

    public Byte getInspDays() {
        return inspDays;
    }

    public void setInspDays(Byte inspDays) {
        this.inspDays = inspDays;
    }

    public String getDoctorAdvice() {
        return doctorAdvice;
    }

    public void setDoctorAdvice(String doctorAdvice) {
        this.doctorAdvice = doctorAdvice;
    }

    public Byte getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(Byte paymentState) {
        this.paymentState = paymentState;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Byte getExecuteState() {
        return executeState;
    }

    public void setExecuteState(Byte executeState) {
        this.executeState = executeState;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}