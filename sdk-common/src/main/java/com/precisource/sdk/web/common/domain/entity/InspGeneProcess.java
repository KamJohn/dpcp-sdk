package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_insp_gene_process
 *
 * @author
 */
public class InspGeneProcess implements Serializable {
    /**
     * 序号
     */
    private String id;

    /**
     * PID
     */
    private String pid;

    /**
     * 患者ID
     */
    private String userId;

    /**
     * 患者hisId,YYGLY02.PATIENTID
     */
    private String hisId;

    /**
     * 检验单号
     */
    private String checkNo;

    /**
     * 患者姓名
     */
    private String name;

    /**
     * 检验时间
     */
    private Date checkTime;

    /**
     * SCFXX01,YYGLE61流水号
     */
    private String rxCode;

    /**
     * 医生
     */
    private String doctorName;

    /**
     * 科室
     */
    private String deptName;

    /**
     * 临床印象
     */
    private String lcyx;

    /**
     * 0:已出报告，1:未出报告
     */
    private String state;

    /**
     * 检验编码
     */
    private String inspCode;

    /**
     * 检验名称
     */
    private String inspName;

    private Date createTime;

    private Date updateTime;

    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHisId() {
        return hisId;
    }

    public void setHisId(String hisId) {
        this.hisId = hisId;
    }

    public String getCheckNo() {
        return checkNo;
    }

    public void setCheckNo(String checkNo) {
        this.checkNo = checkNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLcyx() {
        return lcyx;
    }

    public void setLcyx(String lcyx) {
        this.lcyx = lcyx;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInspCode() {
        return inspCode;
    }

    public void setInspCode(String inspCode) {
        this.inspCode = inspCode;
    }

    public String getInspName() {
        return inspName;
    }

    public void setInspName(String inspName) {
        this.inspName = inspName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}