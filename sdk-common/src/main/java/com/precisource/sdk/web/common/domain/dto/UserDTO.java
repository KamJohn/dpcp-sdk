package com.precisource.sdk.web.common.domain.dto;

/**
 * @author zanxus
 * @version 1.0.0
 * @date 2017-12-06 18:35
 * @description
 */
public class UserDTO {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getAccid() {
        return accid;
    }

    public void setAccid(String accid) {
        this.accid = accid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getJpushAlias() {
        return jpushAlias;
    }

    public void setJpushAlias(String jpushAlias) {
        this.jpushAlias = jpushAlias;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getUserCourseId() {
        return userCourseId;
    }

    public void setUserCourseId(String userCourseId) {
        this.userCourseId = userCourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String id;

    public String tid;

    public String accid;

    public String pid;

    public String jpushAlias;

    public String stage;

    public String idCard;

    public String hospitalId;

    public String userCourseId;

    public String name;

    public String teamId;

}
