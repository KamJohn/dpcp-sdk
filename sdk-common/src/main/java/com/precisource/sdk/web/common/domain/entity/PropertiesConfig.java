package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_properties_config
 *
 * @author
 */
public class PropertiesConfig implements Serializable {
    private String id;

    /**
     * 属性名称
     */
    private String key;

    /**
     * 属性值
     */
    private String value;

    /**
     * 属性描述
     */
    private String description;

    private Date createTime;

    private Date updateTime;

    private Boolean recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }
}