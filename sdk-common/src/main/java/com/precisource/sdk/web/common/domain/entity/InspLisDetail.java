package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_insp_lis_detail
 * @author 
 */
public class InspLisDetail implements Serializable {
    private String id;

    /**
     * 医院ID
     */
    private String hospitalId;

    /**
     * 报告单编码
     */
    private String reportCode;

    /**
     * 处方编码。prescription缩写rx
     */
    private String rxCode;

    /**
     * 处方明细编码
     */
    private String rxDetlCode;

    /**
     * PID
     */
    private String pid;

    /**
     * HIS患者编码
     */
    private String userCode;

    /**
     * 创智患者编码
     */
    private String userExtCode;

    /**
     * 检验项目编码。inspect缩写insp
     */
    private String inspCode;

    /**
     * 检验项目名称
     */
    private String inspName;

    /**
     * 检验项目简称
     */
    private String inspSname;

    /**
     * 检验值
     */
    private String inspValue;

    /**
     * 检验值单位
     */
    private String inspUnit;

    /**
     * 检验值状态
     */
    private Byte inspState;

    /**
     * 参考值
     */
    private String inspRenge;

    /**
     * 同步时间
     */
    private Date createTime;

    private Date updateTime;

    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getRxDetlCode() {
        return rxDetlCode;
    }

    public void setRxDetlCode(String rxDetlCode) {
        this.rxDetlCode = rxDetlCode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserExtCode() {
        return userExtCode;
    }

    public void setUserExtCode(String userExtCode) {
        this.userExtCode = userExtCode;
    }

    public String getInspCode() {
        return inspCode;
    }

    public void setInspCode(String inspCode) {
        this.inspCode = inspCode;
    }

    public String getInspName() {
        return inspName;
    }

    public void setInspName(String inspName) {
        this.inspName = inspName;
    }

    public String getInspSname() {
        return inspSname;
    }

    public void setInspSname(String inspSname) {
        this.inspSname = inspSname;
    }

    public String getInspValue() {
        return inspValue;
    }

    public void setInspValue(String inspValue) {
        this.inspValue = inspValue;
    }

    public String getInspUnit() {
        return inspUnit;
    }

    public void setInspUnit(String inspUnit) {
        this.inspUnit = inspUnit;
    }

    public Byte getInspState() {
        return inspState;
    }

    public void setInspState(Byte inspState) {
        this.inspState = inspState;
    }

    public String getInspRenge() {
        return inspRenge;
    }

    public void setInspRenge(String inspRenge) {
        this.inspRenge = inspRenge;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}