package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_user_basic
 * @author 
 */
public class UserBasic implements Serializable {
    private String id;

    /**
     * 医院id
     */
    private String hospitalId;

    /**
     * 医院名称
     */
    private String hospitalName;

    /**
     * 联盟医院id
     */
    private String allianceHospId;

    /**
     * 联盟医院名称
     */
    private String allianceHospName;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户pid
     */
    private String pid;

    /**
     * 用户姓名
     */
    private String name;

    /**
     * 婚烟状况 已婚:married 未婚：unmarried
     */
    private String marriage;

    /**
     * 家庭关系角色
     */
    private String familyRole;

    /**
     * 性别 男 /女
     */
    private String gender;

    /**
     * 出生日期
     */
    private Date birthday;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 职业
     */
    private String occupation;

    /**
     * 学历
     */
    private String education;

    /**
     * 民族
     */
    private String ethnic;

    /**
     * 住址
     */
    private String address;

    /**
     * 身高 cm
     */
    private Integer height;

    /**
     * 体重 kg
     */
    private String weight;

    /**
     * 是否正在进行中 1是 0否
     */
    private Boolean processing;

    /**
     * 1：注册用户；2：配偶
     */
    private Boolean registFlag;

    private Date createTime;

    private Date updateTime;

    /**
     * 记录有效标识 0有效 1无效
     */
    private Boolean recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getAllianceHospId() {
        return allianceHospId;
    }

    public void setAllianceHospId(String allianceHospId) {
        this.allianceHospId = allianceHospId;
    }

    public String getAllianceHospName() {
        return allianceHospName;
    }

    public void setAllianceHospName(String allianceHospName) {
        this.allianceHospName = allianceHospName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public String getFamilyRole() {
        return familyRole;
    }

    public void setFamilyRole(String familyRole) {
        this.familyRole = familyRole;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEthnic() {
        return ethnic;
    }

    public void setEthnic(String ethnic) {
        this.ethnic = ethnic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Boolean getProcessing() {
        return processing;
    }

    public void setProcessing(Boolean processing) {
        this.processing = processing;
    }

    public Boolean getRegistFlag() {
        return registFlag;
    }

    public void setRegistFlag(Boolean registFlag) {
        this.registFlag = registFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }
}