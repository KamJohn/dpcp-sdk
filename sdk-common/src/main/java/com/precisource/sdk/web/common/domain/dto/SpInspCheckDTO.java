package com.precisource.sdk.web.common.domain.dto;

import java.util.Date;

/**
 * @author xinput
 * @date 2018-11-09 11:35
 */
public class SpInspCheckDTO {

    public String pid;

    public String userId;

    /**
     * 项目编码
     */
    public String inspCode;

    public String gender;

    /**
     * 是否开单: 0未开 1已开
     */
    public Integer rxFlag;

    /**
     * 报告单状态: 0未出 1已出
     */
    public Integer reportFlag;

    public String rxCode;

    public Date rxDate;

    public Date reportDate;

    /**
     * 项目类别：1必审、2可选
     */
    public Integer inspFlag;

    public String rxDoctor;

    public String reportCode;

    public Integer hospitalFlag;
}
