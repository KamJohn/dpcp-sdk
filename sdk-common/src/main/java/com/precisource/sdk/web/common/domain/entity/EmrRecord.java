package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_emr_record
 *
 * @author
 */
public class EmrRecord implements Serializable {
    private String id;

    /**
     * 病历记录编码
     */
    private String emrCode;

    /**
     * 病历记录名称
     */
    private String emrName;

    /**
     * PID
     */
    private String pid;

    /**
     * 患者编码
     */
    private String userCode;

    /**
     * 就诊医生id
     */
    private String doctorCode;

    /**
     * 就诊医生姓名
     */
    private String doctorName;

    /**
     * 就诊时间
     */
    private Date recordTime;

    /**
     * 病历记录类型 0=暂存；1=完成；2=审核
     */
    private Byte recordType;

    /**
     * 病历序号
     */
    private Byte emrSort;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmrCode() {
        return emrCode;
    }

    public void setEmrCode(String emrCode) {
        this.emrCode = emrCode;
    }

    public String getEmrName() {
        return emrName;
    }

    public void setEmrName(String emrName) {
        this.emrName = emrName;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Byte getRecordType() {
        return recordType;
    }

    public void setRecordType(Byte recordType) {
        this.recordType = recordType;
    }

    public Byte getEmrSort() {
        return emrSort;
    }

    public void setEmrSort(Byte emrSort) {
        this.emrSort = emrSort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}