package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_emr_record_detl
 *
 * @author
 */
public class EmrRecordDetl implements Serializable {
    private String id;

    /**
     * 病历记录ID
     */
    private String emrCode;

    /**
     * 病历序号
     */
    private Byte emrSort;

    /**
     * PID
     */
    private String pid;

    /**
     * 患者编码
     */
    private String userCode;

    /**
     * 病历明细项编码
     */
    private String itemCode;

    /**
     * 病历明细项名称
     */
    private String itemName;

    /**
     * 前缀描述
     */
    private String preDesc;

    /**
     * 明细项目内容
     */
    private String itemContent;

    /**
     * 后缀描述
     */
    private String sufDesc;

    /**
     * 父级编码
     */
    private String itemPcode;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmrCode() {
        return emrCode;
    }

    public void setEmrCode(String emrCode) {
        this.emrCode = emrCode;
    }

    public Byte getEmrSort() {
        return emrSort;
    }

    public void setEmrSort(Byte emrSort) {
        this.emrSort = emrSort;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPreDesc() {
        return preDesc;
    }

    public void setPreDesc(String preDesc) {
        this.preDesc = preDesc;
    }

    public String getItemContent() {
        return itemContent;
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
    }

    public String getSufDesc() {
        return sufDesc;
    }

    public void setSufDesc(String sufDesc) {
        this.sufDesc = sufDesc;
    }

    public String getItemPcode() {
        return itemPcode;
    }

    public void setItemPcode(String itemPcode) {
        this.itemPcode = itemPcode;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}