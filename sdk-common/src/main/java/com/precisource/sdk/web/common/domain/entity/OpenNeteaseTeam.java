package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_open_netease_team
 *
 * @author
 */
public class OpenNeteaseTeam implements Serializable {
    private String id;

    /**
     * 网易im群组id
     */
    private String tid;

    /**
     * 群主userId
     */
    private String ownerUserId;

    /**
     * 群主accid
     */
    private String ownerAccid;

    private Date createTime;

    private Date updateTime;

    /**
     * 记录有效标识 0有效 1无效
     */
    private Boolean recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(String ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getOwnerAccid() {
        return ownerAccid;
    }

    public void setOwnerAccid(String ownerAccid) {
        this.ownerAccid = ownerAccid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }
}