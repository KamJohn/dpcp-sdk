package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_doctor_insp_list
 *
 * @author
 */
public class DoctorInspList implements Serializable {
    private String id;

    /**
     * 医院ID
     */
    private String hospitalId;

    /**
     * 报告单编码
     */
    private String reportCode;

    /**
     * 报告名称
     */
    private String reportName;

    /**
     * 报告类型
     */
    private Integer reportType;

    /**
     * 处方编码。prescription缩写rx
     */
    private String rxCode;

    /**
     * 处方明细编码
     */
    private String rxDetlCode;

    /**
     * PID
     */
    private String pid;

    /**
     * HIS患者编码
     */
    private String userCode;

    /**
     * 创智患者编码
     */
    private String userExtCode;

    /**
     * 患者姓名
     */
    private String userName;

    /**
     * 患者年龄
     */
    private String userAge;

    /**
     * 标本种类
     */
    private String specimenType;

    /**
     * 样本编码
     */
    private String sampleCode;

    /**
     * 样本类型
     */
    private String sampleName;

    /**
     * 采样日期
     */
    private Date sampleDate;

    /**
     * 送检日期
     */
    private Date inspDate;

    /**
     * 报告日期
     */
    private Date reportDate;

    /**
     * 临床诊断
     */
    private String diagnosis;

    /**
     * 开单医生
     */
    private String rxDr;

    /**
     * 开单科室编码
     */
    private String rxDeptCode;

    /**
     * 报告医生
     */
    private String reportDr;

    /**
     * 审核医生
     */
    private String reviewDr;

    /**
     * 备注
     */
    private String comment;

    /**
     * 文件url地址
     */
    private String fileUrl;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 创建时间，即备注时间
     */
    private Date createTime;

    /**
     * 同步时间
     */
    private Date updateTime;

    /**
     * 记录是否有效
     */
    private Integer recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getRxDetlCode() {
        return rxDetlCode;
    }

    public void setRxDetlCode(String rxDetlCode) {
        this.rxDetlCode = rxDetlCode;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserExtCode() {
        return userExtCode;
    }

    public void setUserExtCode(String userExtCode) {
        this.userExtCode = userExtCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAge() {
        return userAge;
    }

    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }

    public String getSpecimenType() {
        return specimenType;
    }

    public void setSpecimenType(String specimenType) {
        this.specimenType = specimenType;
    }

    public String getSampleCode() {
        return sampleCode;
    }

    public void setSampleCode(String sampleCode) {
        this.sampleCode = sampleCode;
    }

    public String getSampleName() {
        return sampleName;
    }

    public void setSampleName(String sampleName) {
        this.sampleName = sampleName;
    }

    public Date getSampleDate() {
        return sampleDate;
    }

    public void setSampleDate(Date sampleDate) {
        this.sampleDate = sampleDate;
    }

    public Date getInspDate() {
        return inspDate;
    }

    public void setInspDate(Date inspDate) {
        this.inspDate = inspDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getRxDr() {
        return rxDr;
    }

    public void setRxDr(String rxDr) {
        this.rxDr = rxDr;
    }

    public String getRxDeptCode() {
        return rxDeptCode;
    }

    public void setRxDeptCode(String rxDeptCode) {
        this.rxDeptCode = rxDeptCode;
    }

    public String getReportDr() {
        return reportDr;
    }

    public void setReportDr(String reportDr) {
        this.reportDr = reportDr;
    }

    public String getReviewDr() {
        return reviewDr;
    }

    public void setReviewDr(String reviewDr) {
        this.reviewDr = reviewDr;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getRecordState() {
        return recordState;
    }

    public void setRecordState(Integer recordState) {
        this.recordState = recordState;
    }
}