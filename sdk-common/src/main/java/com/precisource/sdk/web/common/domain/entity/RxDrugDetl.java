package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * t_rx_drug_detl
 *
 * @author
 */
public class RxDrugDetl implements Serializable {
    private String id;

    /**
     * 处方编码
     */
    private String rxCode;

    /**
     * 处方明细编码
     */
    private String rxDetlCode;

    /**
     * 处方类型
     */
    private Byte rxType;

    private String pid;

    private String userCode;

    /**
     * 药品编码
     */
    private String drugCode;

    /**
     * 药品名称
     */
    private String drugName;

    /**
     * 规格
     */
    private String drugSpec;

    /**
     * 数量
     */
    private BigDecimal drugQty;

    /**
     * 数量单位
     */
    private String drugQtyUnit;

    /**
     * 单价
     */
    private BigDecimal drugPrice;

    /**
     * 用量
     */
    private BigDecimal drugDosage;

    /**
     * 用量单位
     */
    private String drugDosageUnit;

    /**
     * 用法
     */
    private String drugUsage;

    /**
     * 频率
     */
    private String drugFreq;

    /**
     * 次数
     */
    private Byte drugTimes;

    /**
     * 给药天数
     */
    private Byte drugDays;

    /**
     * 医嘱内容
     */
    private String doctorAdvice;

    /**
     * 缴费状态
     */
    private Byte paymentState;

    /**
     * 备注
     */
    private String comment;

    /**
     * 执行状态
     */
    private Byte executeState;

    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public String getRxDetlCode() {
        return rxDetlCode;
    }

    public void setRxDetlCode(String rxDetlCode) {
        this.rxDetlCode = rxDetlCode;
    }

    public Byte getRxType() {
        return rxType;
    }

    public void setRxType(Byte rxType) {
        this.rxType = rxType;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugSpec() {
        return drugSpec;
    }

    public void setDrugSpec(String drugSpec) {
        this.drugSpec = drugSpec;
    }

    public BigDecimal getDrugQty() {
        return drugQty;
    }

    public void setDrugQty(BigDecimal drugQty) {
        this.drugQty = drugQty;
    }

    public String getDrugQtyUnit() {
        return drugQtyUnit;
    }

    public void setDrugQtyUnit(String drugQtyUnit) {
        this.drugQtyUnit = drugQtyUnit;
    }

    public BigDecimal getDrugPrice() {
        return drugPrice;
    }

    public void setDrugPrice(BigDecimal drugPrice) {
        this.drugPrice = drugPrice;
    }

    public BigDecimal getDrugDosage() {
        return drugDosage;
    }

    public void setDrugDosage(BigDecimal drugDosage) {
        this.drugDosage = drugDosage;
    }

    public String getDrugDosageUnit() {
        return drugDosageUnit;
    }

    public void setDrugDosageUnit(String drugDosageUnit) {
        this.drugDosageUnit = drugDosageUnit;
    }

    public String getDrugUsage() {
        return drugUsage;
    }

    public void setDrugUsage(String drugUsage) {
        this.drugUsage = drugUsage;
    }

    public String getDrugFreq() {
        return drugFreq;
    }

    public void setDrugFreq(String drugFreq) {
        this.drugFreq = drugFreq;
    }

    public Byte getDrugTimes() {
        return drugTimes;
    }

    public void setDrugTimes(Byte drugTimes) {
        this.drugTimes = drugTimes;
    }

    public Byte getDrugDays() {
        return drugDays;
    }

    public void setDrugDays(Byte drugDays) {
        this.drugDays = drugDays;
    }

    public String getDoctorAdvice() {
        return doctorAdvice;
    }

    public void setDoctorAdvice(String doctorAdvice) {
        this.doctorAdvice = doctorAdvice;
    }

    public Byte getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(Byte paymentState) {
        this.paymentState = paymentState;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Byte getExecuteState() {
        return executeState;
    }

    public void setExecuteState(Byte executeState) {
        this.executeState = executeState;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}