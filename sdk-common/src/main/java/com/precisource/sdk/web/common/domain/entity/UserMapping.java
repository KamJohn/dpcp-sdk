package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * r_user_mapping
 *
 * @author
 */
public class UserMapping implements Serializable {
    private String id;

    /**
     * 医院id
     */
    private String hospitalId;

    /**
     * 用户身份证号
     */
    private String idCard;

    /**
     * 自建用户ID
     */
    private String userId;

    /**
     * 启峰用户ID(HIS)
     */
    private String hisId;

    /**
     * 创智用户ID
     */
    private String dhisId;

    /**
     * 用户pid
     */
    private String pid;

    private Date createTime;

    private Date updateTime;

    /**
     * 记录有效标识 0有效 1无效
     */
    private Boolean recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHisId() {
        return hisId;
    }

    public void setHisId(String hisId) {
        this.hisId = hisId;
    }

    public String getDhisId() {
        return dhisId;
    }

    public void setDhisId(String dhisId) {
        this.dhisId = dhisId;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getRecordState() {
        return recordState;
    }

    public void setRecordState(Boolean recordState) {
        this.recordState = recordState;
    }

    @Override
    public String toString() {
        return "UserMapping{" +
                "id='" + id + '\'' +
                ", hospitalId='" + hospitalId + '\'' +
                ", idCard='" + idCard + '\'' +
                ", userId='" + userId + '\'' +
                ", hisId='" + hisId + '\'' +
                ", dhisId='" + dhisId + '\'' +
                ", pid='" + pid + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", recordState=" + recordState +
                '}';
    }
}