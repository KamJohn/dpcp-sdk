package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;

/**
 * t_doctor_insp_pacs_detail
 *
 * @author
 */
public class DoctorInspPacsDetailWithBLOBs extends DoctorInspPacsDetail implements Serializable {
    /**
     * 检查项目描述
     */
    private String inspDesc;

    /**
     * 检查项目结果
     */
    private String inspDx;

    private static final long serialVersionUID = 1L;

    public String getInspDesc() {
        return inspDesc;
    }

    public void setInspDesc(String inspDesc) {
        this.inspDesc = inspDesc;
    }

    public String getInspDx() {
        return inspDx;
    }

    public void setInspDx(String inspDx) {
        this.inspDx = inspDx;
    }
}