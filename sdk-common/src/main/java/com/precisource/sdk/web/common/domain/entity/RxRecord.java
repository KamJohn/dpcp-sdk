package com.precisource.sdk.web.common.domain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * t_rx_record
 *
 * @author
 */
public class RxRecord implements Serializable {
    private String id;

    /**
     * PID
     */
    private String pid;

    /**
     * 患者编码
     */
    private String userCode;

    /**
     * 挂号单号
     */
    private String registerCode;

    /**
     * 模板
     */
    private String templateId;

    /**
     * 医生编码
     */
    private String doctorCode;

    /**
     * 处方编码
     */
    private String rxCode;

    /**
     * 开方日期
     */
    private Date rxDate;

    /**
     * 处方类别
     */
    private Byte rxType;

    /**
     * 诊断
     */
    private String diagnosis;

    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 记录有效状态
     */
    private Byte recordState;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getRxCode() {
        return rxCode;
    }

    public void setRxCode(String rxCode) {
        this.rxCode = rxCode;
    }

    public Date getRxDate() {
        return rxDate;
    }

    public void setRxDate(Date rxDate) {
        this.rxDate = rxDate;
    }

    public Byte getRxType() {
        return rxType;
    }

    public void setRxType(Byte rxType) {
        this.rxType = rxType;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getRecordState() {
        return recordState;
    }

    public void setRecordState(Byte recordState) {
        this.recordState = recordState;
    }
}